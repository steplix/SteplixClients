# SteplixClients

En este repositorio se encuentran absolutamente todos los `clientes` de la empresa `Steplix` que facilitan el trabajo en los diferentes proyectos.


## Bash Client

La documentación de la instalación y el uso se encuentran en la sección [Wiki de este proyecto](https://gitlab.com/steplix/SteplixClients/-/wikis/Bash-Client-%7C-Installation).

Es un cliente de línea de comandos para `Ubuntu 16.04` que permite `install` el software y las dependencias que utilizamos en todos los proyectos, asi como notificar sencillamente `steps` de los `pipelines`, entre otras funcionalidades.

Por citar algunos ejemplos:

````sh
$ steplix help
$ steplix install android
$ steplix gitlab slack
$ steplix amazon devicefarm
$ steplix setup git-aliases
$ steplix setup utf-8
````


## Fast Install

```sh
wget -O - https://gitlab.com/steplix/SteplixClients/-/raw/master/bash-client/install.sh | bash > /dev/null 2>&1
```
