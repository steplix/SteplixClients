#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix strings application
# @author      	Steplix
# @description	Strings Helper
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "argument" "bool-argument" "trim")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/bool-argument.sh"
source "${commands_path}/argument.sh"
source "${commands_path}/trim.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

