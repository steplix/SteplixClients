#!/usr/bin/env bash

argument() {
	
	local key=$1
	shift
	local args=$@

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${args//--/|}"

	for element in "${my_array[@]}"; do

		# get the keypath
		if [[ $element == *"${key}="* ]]; then
			value=${element//${key}=/}
			value=$(${me} strings trim ${value})
			echo ${value}
			exit
		fi

	done

	echo ""
}