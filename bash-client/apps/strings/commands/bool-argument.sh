#!/usr/bin/env bash

bool-argument() {
	
	local key=$1
	shift
	local args=$@

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${args//--/|}"

	for element in "${my_array[@]}"; do
		# trim element
		element=$(${me} strings trim ${element});

		# get the keypath
		if [ "${element}" = "${key}" ]; then
			echo "true"
			exit
		fi

	done

	echo "false"
}