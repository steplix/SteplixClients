#!/usr/bin/env bash

trim() {
	
	# the full quote is composed by all args
	local args=$@

	# print out the string without the quotes
	echo $args | sed 's/ *$//g'
}