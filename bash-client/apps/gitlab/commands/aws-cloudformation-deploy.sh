#!/usr/bin/env bash

# Available variables
#   Gitlab
#     * CI_PROJECT_PATH_SLUG               See https://docs.gitlab.com/ee/ci/variables/README.html#list-all-environment-variables#CI_PROJECT_PATH_SLUG
#     * CI_COMMIT_SHORT_SHA                See https://docs.gitlab.com/ee/ci/variables/README.html#list-all-environment-variables#CI_COMMIT_SHORT_SHA
#     * CI_COMMIT_BRANCH                   See https://docs.gitlab.com/ee/ci/variables/README.html#list-all-environment-variables#CI_COMMIT_BRANCH
#     * CI_COMMIT_TAG                      See https://docs.gitlab.com/ee/ci/variables/README.html#list-all-environment-variables#CI_COMMIT_TAG
#
#   Environment
#     aws cloud formation
#       * AWS_CLOUDFORMATION_STACK_NAME    AWS Cloud formation Stack Name
#       * AWS_CLOUDFORMATION_TEMPLATE      AWS Cloud formation Template file path. Default "file://cloudformation/cloudformation.yml"
#     aws credentials
#       * AWS_DEPLOY_ACCOUNT_ID            AWS Credentials Account ID
#       * AWS_DEPLOY_ACCESS_KEY            AWS Credentials Access Key
#       * AWS_DEPLOY_SECRET_KEY            AWS Credentials Secret Key
#       * AWS_DEPLOY_REGION                AWS Credentials Region
#     aws cloud
#       * AWS_DEPLOY_VPC_ID                AWS VPC ID
#       * AWS_DEPLOY_LOADBALANCER_ARN      AWS Load balance ARN
#     aws container
#       * AWS_CONTAINER_NAME               AWS ECS Service name and Task definition name
#       * AWS_CONTAINER_CLUSTER            AWS ECS Cluster name
#       * AWS_CONTAINER_IMAGE              AWS ECR Image name
#       * AWS_SERVICE_NAMESPACE            AWS ECS Service Discovery DNS Namespace
#     aws ecr
#       * ENV_DOCKER_AWS_ACCOUNT_ID        Override url with AWS ECR Account ID
#       * ENV_DOCKER_AWS_REGION            Override url with AWS ECR Region
#
#   Variables can be configured by environment.
#     Syntax
#       * <variable name>_<environment>
#     Example
#       * AWS_CONTAINER_IMAGE_TESTING
#     Available environments
#       * TESTING
#       * STAGING
#       * PRODUCTION
#       * LAB
#       * *OTHER* when "environment" argument is declared
#     Available variable names
#       * AWS_CLOUDFORMATION_STACK_NAME
#       * AWS_CLOUDFORMATION_TEMPLATE
#       * AWS_DEPLOY_ACCOUNT_ID
#       * AWS_DEPLOY_ACCESS_KEY
#       * AWS_DEPLOY_SECRET_KEY
#       * AWS_DEPLOY_REGION
#       * AWS_DEPLOY_VPC_ID
#       * AWS_DEPLOY_LOADBALANCER_ARN
#       * ENV_DOCKER_AWS_ACCOUNT_ID
#       * ENV_DOCKER_AWS_REGION
#       * AWS_CONTAINER_NAME
#       * AWS_CONTAINER_CLUSTER
#       * AWS_SERVICE_NAMESPACE
#       * AWS_CONTAINER_IMAGE
#
#   Variables to which the environment will be concatenated at the end of the value.
#     **********************************************
#     *** Only if AWS_APPEND_ENVIRONMENT == true ***
#     **********************************************
#
#     Syntax
#       * <variable value>-<lowercase environment>
#     Example
#       * ctech-currencies-testing
#     Available environments
#       * testing
#       * staging
#       * production
#       * lab
#       * *other* 
#     Variable names affected by this condition
#       * AWS_CLOUDFORMATION_STACK_NAME
#
# Arguments
#   * branch                               Indicate deploy branch. Default is CI_COMMIT_BRANCH
#   * image                                Indicate deploy AWS ECR Image name. Default is AWS_CONTAINER_IMAGE
#   * tag                                  Indicate deploy AWS ECR Image tag. Default is AWS_CONTAINER_IMAGE
#   * environment                          Indicate deploy environment. Default is taken from "branch" argument.


aws-cloudformation-deploy() {

    # check gitlab job
    ${me} gitlab check
    if [ ! $? = 0 ]; then exit $?; fi

    # variables
    local titleBranch=""
    local parameters=""


    # keep args
    local args=$@

    # retrieve args
    local currentBranch=$(${me} strings trim ${CI_COMMIT_BRANCH})
    local branch=$(${me} strings argument branch $args)
    local image=$(${me} strings argument image $args)
    local tag=$(${me} strings argument tag $args)
    local environment=$(${me} strings argument environment $args)


    # keep environment variables

    # aws cloud formation
    local AWS_CLOUDFORMATION_STACK_NAME=$(${me} strings trim ${AWS_CLOUDFORMATION_STACK_NAME})
    local AWS_CLOUDFORMATION_TEMPLATE=$(${me} strings trim ${AWS_CLOUDFORMATION_TEMPLATE})

    # aws credentials
    local AWS_DEPLOY_ACCOUNT_ID=$(${me} strings trim ${AWS_DEPLOY_ACCOUNT_ID})
    local AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY})
    local AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY})
    local AWS_DEPLOY_REGION=$(${me} strings trim ${AWS_DEPLOY_REGION})

    # aws cloud
    local AWS_DEPLOY_VPC_ID=$(${me} strings trim ${AWS_DEPLOY_VPC_ID})
    local AWS_DEPLOY_LOADBALANCER_ARN=$(${me} strings trim ${AWS_DEPLOY_LOADBALANCER_ARN})

    # aws container
    local AWS_CONTAINER_NAME=$(${me} strings trim ${AWS_CONTAINER_NAME})
    local AWS_CONTAINER_CLUSTER=$(${me} strings trim ${AWS_CONTAINER_CLUSTER})
    local AWS_SERVICE_NAMESPACE=$(${me} strings trim ${AWS_SERVICE_NAMESPACE})
    local AWS_CONTAINER_IMAGE=$(${me} strings trim ${AWS_CONTAINER_IMAGE})
    local AWS_CONTAINER_TAG=$(${me} strings trim ${CI_COMMIT_TAG})

    # aws ecr
    local ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${ENV_DOCKER_AWS_ACCOUNT_ID})
    local ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION})

    # git
    local GIT_COMMIT_SHORT_SHA=$(${me} strings trim ${CI_COMMIT_SHORT_SHA})

    # ENVIRONMENT
    local UPPER_ENV=$(to_upper ${environment})

    # check options args

    # check dynamic variables by environment

    # TESTING
    if [ "${currentBranch}" == "testing" ]; then
        titleBranch="Testing"
        branch="testing"

        if [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME_TESTING}" ]; then
            AWS_CLOUDFORMATION_STACK_NAME=$(${me} strings trim ${AWS_CLOUDFORMATION_STACK_NAME_TESTING})
        elif [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME}" ] && [ "${AWS_APPEND_ENVIRONMENT}" == "true" ]; then
            AWS_CLOUDFORMATION_STACK_NAME="${AWS_CLOUDFORMATION_STACK_NAME}-testing"
        fi
        if [ ! -z "${AWS_CLOUDFORMATION_TEMPLATE_TESTING}" ]; then
            AWS_CLOUDFORMATION_TEMPLATE=$(${me} strings trim ${AWS_CLOUDFORMATION_TEMPLATE_TESTING})
        fi
        if [ ! -z "${AWS_DEPLOY_ACCOUNT_ID_TESTING}" ]; then
            AWS_DEPLOY_ACCOUNT_ID=$(${me} strings trim ${AWS_DEPLOY_ACCOUNT_ID_TESTING})
        fi
        if [ ! -z "${AWS_DEPLOY_ACCESS_KEY_TESTING}" ]; then
            AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY_TESTING})
        fi
        if [ ! -z "${AWS_DEPLOY_SECRET_KEY_TESTING}" ]; then
            AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY_TESTING})
        fi
        if [ ! -z "${AWS_DEPLOY_REGION_TESTING}" ]; then
            AWS_DEPLOY_REGION=$(${me} strings trim ${AWS_DEPLOY_REGION_TESTING})
        fi
        if [ ! -z "${AWS_DEPLOY_VPC_ID_TESTING}" ]; then
            AWS_DEPLOY_VPC_ID=$(${me} strings trim ${AWS_DEPLOY_VPC_ID_TESTING})
        fi
        if [ ! -z "${AWS_DEPLOY_LOADBALANCER_ARN_TESTING}" ]; then
            AWS_DEPLOY_LOADBALANCER_ARN=$(${me} strings trim ${AWS_DEPLOY_LOADBALANCER_ARN_TESTING})
        fi
        if [ ! -z "${AWS_CONTAINER_NAME_TESTING}" ]; then
            AWS_CONTAINER_NAME=$(${me} strings trim ${AWS_CONTAINER_NAME_TESTING})
        fi
        if [ ! -z "${AWS_CONTAINER_CLUSTER_TESTING}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${AWS_CONTAINER_CLUSTER_TESTING})
        fi
        if [ ! -z "${AWS_SERVICE_NAMESPACE_TESTING}" ]; then
            AWS_SERVICE_NAMESPACE=$(${me} strings trim ${AWS_SERVICE_NAMESPACE_TESTING})
        fi
        if [ ! -z "${AWS_CONTAINER_IMAGE_TESTING}" ]; then
            AWS_CONTAINER_IMAGE=$(${me} strings trim ${AWS_CONTAINER_IMAGE_TESTING})
        fi
        if [ ! -z "${ENV_DOCKER_AWS_ACCOUNT_ID_TESTING}" ]; then
            ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${ENV_DOCKER_AWS_ACCOUNT_ID_TESTING})
        fi
        if [ ! -z "${ENV_DOCKER_AWS_REGION_TESTING}" ]; then
            ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_TESTING})
        fi

    # STAGING
    elif [ "${branch}" == "staging" ]; then
        titleBranch="Staging"
        branch="staging"

        if [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME_STAGING}" ]; then
            AWS_CLOUDFORMATION_STACK_NAME=$(${me} strings trim ${AWS_CLOUDFORMATION_STACK_NAME_STAGING})
        elif [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME}" ] && [ "${AWS_APPEND_ENVIRONMENT}" == "true" ]; then
            AWS_CLOUDFORMATION_STACK_NAME="${AWS_CLOUDFORMATION_STACK_NAME}-staging"
        fi
        if [ ! -z "${AWS_CLOUDFORMATION_TEMPLATE_STAGING}" ]; then
            AWS_CLOUDFORMATION_TEMPLATE=$(${me} strings trim ${AWS_CLOUDFORMATION_TEMPLATE_STAGING})
        fi
        if [ ! -z "${AWS_DEPLOY_ACCOUNT_ID_STAGING}" ]; then
            AWS_DEPLOY_ACCOUNT_ID=$(${me} strings trim ${AWS_DEPLOY_ACCOUNT_ID_STAGING})
        fi
        if [ ! -z "${AWS_DEPLOY_ACCESS_KEY_STAGING}" ]; then
            AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY_STAGING})
        fi
        if [ ! -z "${AWS_DEPLOY_SECRET_KEY_STAGING}" ]; then
            AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY_STAGING})
        fi
        if [ ! -z "${AWS_DEPLOY_REGION_STAGING}" ]; then
            AWS_DEPLOY_REGION=$(${me} strings trim ${AWS_DEPLOY_REGION_STAGING})
        fi
        if [ ! -z "${AWS_DEPLOY_VPC_ID_STAGING}" ]; then
            AWS_DEPLOY_VPC_ID=$(${me} strings trim ${AWS_DEPLOY_VPC_ID_STAGING})
        fi
        if [ ! -z "${AWS_DEPLOY_LOADBALANCER_ARN_STAGING}" ]; then
            AWS_DEPLOY_LOADBALANCER_ARN=$(${me} strings trim ${AWS_DEPLOY_LOADBALANCER_ARN_STAGING})
        fi
        if [ ! -z "${AWS_CONTAINER_NAME_STAGING}" ]; then
            AWS_CONTAINER_NAME=$(${me} strings trim ${AWS_CONTAINER_NAME_STAGING})
        fi
        if [ ! -z "${AWS_CONTAINER_CLUSTER_STAGING}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${AWS_CONTAINER_CLUSTER_STAGING})
        fi
        if [ ! -z "${AWS_SERVICE_NAMESPACE_STAGING}" ]; then
            AWS_SERVICE_NAMESPACE=$(${me} strings trim ${AWS_SERVICE_NAMESPACE_STAGING})
        fi
        if [ ! -z "${AWS_CONTAINER_IMAGE_STAGING}" ]; then
            AWS_CONTAINER_IMAGE=$(${me} strings trim ${AWS_CONTAINER_IMAGE_STAGING})
        fi
        if [ ! -z "${ENV_DOCKER_AWS_ACCOUNT_ID_STAGING}" ]; then
            ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${ENV_DOCKER_AWS_ACCOUNT_ID_STAGING})
        fi
        if [ ! -z "${ENV_DOCKER_AWS_REGION_STAGING}" ]; then
            ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_STAGING})
        fi

    # PRODUCTION
    elif [ "${branch}" == "master" ] || [ "${branch}" == "production" ]; then
        titleBranch="Production"
        branch="production"

        if [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME_PRODUCTION}" ]; then
            AWS_CLOUDFORMATION_STACK_NAME=$(${me} strings trim ${AWS_CLOUDFORMATION_STACK_NAME_PRODUCTION})
        elif [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME}" ] && [ "${AWS_APPEND_ENVIRONMENT}" == "true" ]; then
            AWS_CLOUDFORMATION_STACK_NAME="${AWS_CLOUDFORMATION_STACK_NAME}-production"
        fi
        if [ ! -z "${AWS_CLOUDFORMATION_TEMPLATE_PRODUCTION}" ]; then
            AWS_CLOUDFORMATION_TEMPLATE=$(${me} strings trim ${AWS_CLOUDFORMATION_TEMPLATE_PRODUCTION})
        fi
        if [ ! -z "${AWS_DEPLOY_ACCOUNT_ID_PRODUCTION}" ]; then
            AWS_DEPLOY_ACCOUNT_ID=$(${me} strings trim ${AWS_DEPLOY_ACCOUNT_ID_PRODUCTION})
        fi
        if [ ! -z "${AWS_DEPLOY_ACCESS_KEY_PRODUCTION}" ]; then
            AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY_PRODUCTION})
        fi
        if [ ! -z "${AWS_DEPLOY_SECRET_KEY_PRODUCTION}" ]; then
            AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY_PRODUCTION})
        fi
        if [ ! -z "${AWS_DEPLOY_REGION_PRODUCTION}" ]; then
            AWS_DEPLOY_REGION=$(${me} strings trim ${AWS_DEPLOY_REGION_PRODUCTION})
        fi
        if [ ! -z "${AWS_DEPLOY_VPC_ID_PRODUCTION}" ]; then
            AWS_DEPLOY_VPC_ID=$(${me} strings trim ${AWS_DEPLOY_VPC_ID_PRODUCTION})
        fi
        if [ ! -z "${AWS_DEPLOY_LOADBALANCER_ARN_PRODUCTION}" ]; then
            AWS_DEPLOY_LOADBALANCER_ARN=$(${me} strings trim ${AWS_DEPLOY_LOADBALANCER_ARN_PRODUCTION})
        fi
        if [ ! -z "${AWS_CONTAINER_NAME_PRODUCTION}" ]; then
            AWS_CONTAINER_NAME=$(${me} strings trim ${AWS_CONTAINER_NAME_PRODUCTION})
        fi
        if [ ! -z "${AWS_CONTAINER_CLUSTER_PRODUCTION}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${AWS_CONTAINER_CLUSTER_PRODUCTION})
        fi
        if [ ! -z "${AWS_SERVICE_NAMESPACE_PRODUCTION}" ]; then
            AWS_SERVICE_NAMESPACE=$(${me} strings trim ${AWS_SERVICE_NAMESPACE_PRODUCTION})
        fi
        if [ ! -z "${AWS_CONTAINER_IMAGE_PRODUCTION}" ]; then
            AWS_CONTAINER_IMAGE=$(${me} strings trim ${AWS_CONTAINER_IMAGE_PRODUCTION})
        fi
        if [ ! -z "${ENV_DOCKER_AWS_ACCOUNT_ID_PRODUCTION}" ]; then
            ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${ENV_DOCKER_AWS_ACCOUNT_ID_PRODUCTION})
        fi
        if [ ! -z "${ENV_DOCKER_AWS_REGION_PRODUCTION}" ]; then
            ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_PRODUCTION})
        fi

    elif [ "${currentBranch}" == "lab" ]; then
        titleBranch="Lab"
        branch="lab"

        if [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME_LAB}" ]; then
            AWS_CLOUDFORMATION_STACK_NAME=$(${me} strings trim ${AWS_CLOUDFORMATION_STACK_NAME_LAB})
        elif [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME}" ] && [ "${AWS_APPEND_ENVIRONMENT}" == "true" ]; then
            AWS_CLOUDFORMATION_STACK_NAME="${AWS_CLOUDFORMATION_STACK_NAME}-lab"
        fi
        if [ ! -z "${AWS_CLOUDFORMATION_TEMPLATE_LAB}" ]; then
            AWS_CLOUDFORMATION_TEMPLATE=$(${me} strings trim ${AWS_CLOUDFORMATION_TEMPLATE_LAB})
        fi
        if [ ! -z "${AWS_DEPLOY_ACCOUNT_ID_LAB}" ]; then
            AWS_DEPLOY_ACCOUNT_ID=$(${me} strings trim ${AWS_DEPLOY_ACCOUNT_ID_LAB})
        fi
        if [ ! -z "${AWS_DEPLOY_ACCESS_KEY_LAB}" ]; then
            AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY_LAB})
        fi
        if [ ! -z "${AWS_DEPLOY_SECRET_KEY_LAB}" ]; then
            AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY_LAB})
        fi
        if [ ! -z "${AWS_DEPLOY_REGION_LAB}" ]; then
            AWS_DEPLOY_REGION=$(${me} strings trim ${AWS_DEPLOY_REGION_LAB})
        fi
        if [ ! -z "${AWS_DEPLOY_VPC_ID_LAB}" ]; then
            AWS_DEPLOY_VPC_ID=$(${me} strings trim ${AWS_DEPLOY_VPC_ID_LAB})
        fi
        if [ ! -z "${AWS_DEPLOY_LOADBALANCER_ARN_LAB}" ]; then
            AWS_DEPLOY_LOADBALANCER_ARN=$(${me} strings trim ${AWS_DEPLOY_LOADBALANCER_ARN_LAB})
        fi
        if [ ! -z "${AWS_CONTAINER_NAME_LAB}" ]; then
            AWS_CONTAINER_NAME=$(${me} strings trim ${AWS_CONTAINER_NAME_LAB})
        fi
        if [ ! -z "${AWS_CONTAINER_CLUSTER_LAB}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${AWS_CONTAINER_CLUSTER_LAB})
        fi
        if [ ! -z "${AWS_SERVICE_NAMESPACE_LAB}" ]; then
            AWS_SERVICE_NAMESPACE=$(${me} strings trim ${AWS_SERVICE_NAMESPACE_LAB})
        fi
        if [ ! -z "${AWS_CONTAINER_IMAGE_LAB}" ]; then
            AWS_CONTAINER_IMAGE=$(${me} strings trim ${AWS_CONTAINER_IMAGE_LAB})
        fi
        if [ ! -z "${ENV_DOCKER_AWS_ACCOUNT_ID_LAB}" ]; then
            ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${ENV_DOCKER_AWS_ACCOUNT_ID_LAB})
        fi
        if [ ! -z "${ENV_DOCKER_AWS_REGION_LAB}" ]; then
            ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_LAB})
        fi

    fi

    # Variables defined by environment argument
    if [ ! -z "${UPPER_ENV}" ]; then
        titleBranch=$(to_title ${environment})

        local AWS_CLOUDFORMATION_STACK_NAME_env="AWS_CLOUDFORMATION_STACK_NAME_$UPPER_ENV"
        local AWS_CLOUDFORMATION_TEMPLATE_env="AWS_CLOUDFORMATION_TEMPLATE_$UPPER_ENV"
        local AWS_DEPLOY_ACCOUNT_ID_env="AWS_DEPLOY_ACCOUNT_ID_$UPPER_ENV"
        local AWS_DEPLOY_ACCESS_KEY_env="AWS_DEPLOY_ACCESS_KEY_$UPPER_ENV"
        local AWS_DEPLOY_SECRET_KEY_env="AWS_DEPLOY_SECRET_KEY_$UPPER_ENV"
        local AWS_DEPLOY_REGION_env="AWS_DEPLOY_REGION_$UPPER_ENV"
        local AWS_DEPLOY_VPC_ID_env="AWS_DEPLOY_VPC_ID_$UPPER_ENV"
        local AWS_DEPLOY_LOADBALANCER_ARN_env="AWS_DEPLOY_LOADBALANCER_ARN_$UPPER_ENV"
        local AWS_CONTAINER_NAME_env="AWS_CONTAINER_NAME_$UPPER_ENV"
        local AWS_CONTAINER_CLUSTER_env="AWS_CONTAINER_CLUSTER_$UPPER_ENV"
        local AWS_SERVICE_NAMESPACE_env="AWS_SERVICE_NAMESPACE_$UPPER_ENV"
        local AWS_CONTAINER_IMAGE_env="AWS_CONTAINER_IMAGE_$UPPER_ENV"
        local ENV_DOCKER_AWS_ACCOUNT_ID_env="ENV_DOCKER_AWS_ACCOUNT_ID_$UPPER_ENV"
        local ENV_DOCKER_AWS_REGION_env="ENV_DOCKER_AWS_REGION_$UPPER_ENV"

        if [ ! -z "${!AWS_CLOUDFORMATION_STACK_NAME_env}" ]; then
            AWS_CLOUDFORMATION_STACK_NAME=$(${me} strings trim ${!AWS_CLOUDFORMATION_STACK_NAME_env})
        elif [ ! -z "${AWS_CLOUDFORMATION_STACK_NAME}" ] && [ "${AWS_APPEND_ENVIRONMENT}" == "true" ]; then
            AWS_CLOUDFORMATION_STACK_NAME="${AWS_CLOUDFORMATION_STACK_NAME}-${environment}"
        fi
        if [ ! -z "${!AWS_CLOUDFORMATION_TEMPLATE_env}" ]; then
            AWS_CLOUDFORMATION_TEMPLATE=$(${me} strings trim ${!AWS_CLOUDFORMATION_TEMPLATE_env})
        fi
        if [ ! -z "${!AWS_DEPLOY_ACCOUNT_ID_env}" ]; then
            AWS_DEPLOY_ACCOUNT_ID=$(${me} strings trim ${!AWS_DEPLOY_ACCOUNT_ID_env})
        fi
        if [ ! -z "${!AWS_DEPLOY_ACCESS_KEY_env}" ]; then
            AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${!AWS_DEPLOY_ACCESS_KEY_env})
        fi
        if [ ! -z "${!AWS_DEPLOY_SECRET_KEY_env}" ]; then
            AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${!AWS_DEPLOY_SECRET_KEY_env})
        fi
        if [ ! -z "${!AWS_DEPLOY_REGION_env}" ]; then
            AWS_DEPLOY_REGION=$(${me} strings trim ${!AWS_DEPLOY_REGION_env})
        fi
        if [ ! -z "${!AWS_DEPLOY_VPC_ID_env}" ]; then
            AWS_DEPLOY_VPC_ID=$(${me} strings trim ${!AWS_DEPLOY_VPC_ID_env})
        fi
        if [ ! -z "${!AWS_DEPLOY_LOADBALANCER_ARN_env}" ]; then
            AWS_DEPLOY_LOADBALANCER_ARN=$(${me} strings trim ${!AWS_DEPLOY_LOADBALANCER_ARN_env})
        fi
        if [ ! -z "${!AWS_CONTAINER_NAME_env}" ]; then
            AWS_CONTAINER_NAME=$(${me} strings trim ${!AWS_CONTAINER_NAME_env})
        fi
        if [ ! -z "${!AWS_CONTAINER_CLUSTER_env}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${!AWS_CONTAINER_CLUSTER_env})
        fi
        if [ ! -z "${!AWS_SERVICE_NAMESPACE_env}" ]; then
            AWS_SERVICE_NAMESPACE=$(${me} strings trim ${!AWS_SERVICE_NAMESPACE_env})
        fi
        if [ ! -z "${!AWS_CONTAINER_IMAGE_env}" ]; then
            AWS_CONTAINER_IMAGE=$(${me} strings trim ${!AWS_CONTAINER_IMAGE_env})
        fi
        if [ ! -z "${!ENV_DOCKER_AWS_ACCOUNT_ID_env}" ]; then
            ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${!ENV_DOCKER_AWS_ACCOUNT_ID_env})
        fi
        if [ ! -z "${!ENV_DOCKER_AWS_REGION_env}" ]; then
            ENV_DOCKER_AWS_REGION=$(${me} strings trim ${!ENV_DOCKER_AWS_REGION_env})
        fi

    fi

    # prepare variables

    # prepare aws container
    # prepare aws container tag
    if [ -z "${tag}" ]; then
        if [ "${branch}" == "testing" ] || [ "${branch}" == "lab" ]; then
            tag="${GIT_COMMIT_SHORT_SHA}"
        else 
            tag="${AWS_CONTAINER_TAG}"
        fi
    fi

    # prepare aws container name
    if [ -z "${AWS_CONTAINER_NAME}" ]; then
        # retrocompatibility with all environment variable names
        if [ ! -z "${ENV_AWS_ECS_NAME}" ]; then
            AWS_CONTAINER_NAME=$(${me} strings trim ${ENV_AWS_ECS_NAME})
        else
            # default value
            AWS_CONTAINER_NAME="${CI_PROJECT_PATH_SLUG}"
        fi
    fi

    # prepare aws container cluster (retrocompatibility with old env names)
    if [ -z "${AWS_CONTAINER_CLUSTER}" ]; then
        # retrocompatibility with all environment variable names
        if [ ! -z "${ENV_DOCKER_CLUSTER}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER})
        fi
        if [ "${branch}" == "testing" ] && [ ! -z "${ENV_DOCKER_CLUSTER_TESTING}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER_TESTING})
        elif [ "${branch}" == "staging" ] && [ ! -z "${ENV_DOCKER_CLUSTER_STAGING}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER_STAGING})
        elif [ "${branch}" == "production" ] && [ ! -z "${ENV_DOCKER_CLUSTER_PRODUCTION}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER_PRODUCTION})
        elif [ "${branch}" == "lab" ] && [ ! -z "${ENV_DOCKER_CLUSTER_LAB}" ]; then
            AWS_CONTAINER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER_LAB})
        else
            ${logger} error "cannot setup AWS_CONTAINER_CLUSTER environment variable."
            exit 1
        fi
    fi

    # prepare aws container image
    if [ -z "${AWS_CONTAINER_IMAGE}" ]; then
        # retrocompatibility with all environment variable names
        if [ ! -z "${ENV_AWS_ECR_NAME}" ]; then
            AWS_CONTAINER_IMAGE=$(${me} strings trim ${ENV_AWS_ECR_NAME})
        else
            # default value
            AWS_CONTAINER_IMAGE="${CI_PROJECT_PATH_SLUG}"
        fi
    fi
    if [ -z "${image}" ]; then
        image="${AWS_CONTAINER_IMAGE}"
    fi

    # prepare aws cloud formation
    if [ -z "${AWS_CLOUDFORMATION_STACK_NAME}" ]; then
        AWS_CLOUDFORMATION_STACK_NAME="${AWS_CONTAINER_NAME}"
        if [ "${AWS_APPEND_ENVIRONMENT}" == "true" ] && [ ! -z "${environment}" ]; then
            AWS_CLOUDFORMATION_STACK_NAME="${AWS_CLOUDFORMATION_STACK_NAME}-${environment}"
        elif [ "${AWS_APPEND_ENVIRONMENT}" == "true" ]; then
            AWS_CLOUDFORMATION_STACK_NAME="${AWS_CLOUDFORMATION_STACK_NAME}-${branch}"
        fi
    fi
    if [ -z "${AWS_CLOUDFORMATION_TEMPLATE}" ]; then
        AWS_CLOUDFORMATION_TEMPLATE="./cloudformation/cloudformation.yml"
    fi

    # prepare args
    image="${image}:${tag}"

    # prepare ecr aws account id
    if [ -z "${ENV_DOCKER_AWS_ACCOUNT_ID}" ]; then
        ENV_DOCKER_AWS_ACCOUNT_ID="${AWS_DEPLOY_ACCOUNT_ID}"
    fi
    if [ -z "${ENV_DOCKER_AWS_REGION}" ]; then
        ENV_DOCKER_AWS_REGION="${AWS_DEPLOY_REGION}"
    fi

    # go go go

    # setup amazon for deployment
    ${me} setup amazon-by-set --region=${AWS_DEPLOY_REGION} --access_key=${AWS_DEPLOY_ACCESS_KEY} --secret_key=${AWS_DEPLOY_SECRET_KEY}

        # check if everything is ok
        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot setup amazon."
            ${logger} note "try to install aws with the following command: steplix install amazon"
            exit 1
        fi

    # prepare parameters
    parameters="Environment=${branch}"
    if [ ! -z "${environment}" ]; then
        parameters="Environment=${environment}"
    fi
    parameters="${parameters} ParameterSuffix=${titleBranch}"
    parameters="${parameters} EnvAwsVpcId=${AWS_DEPLOY_VPC_ID}"
    parameters="${parameters} EnvAwsLoadBalancerARN=${AWS_DEPLOY_LOADBALANCER_ARN}"
    parameters="${parameters} EnvAwsRegion=${AWS_DEPLOY_REGION}"
    parameters="${parameters} EnvAwsAccount=${AWS_DEPLOY_ACCOUNT_ID}"
    parameters="${parameters} EnvAwsECRImage=${image}"
    parameters="${parameters} EnvAwsECSName=${AWS_CONTAINER_NAME}"
    parameters="${parameters} EnvAwsECSCluster=${AWS_CONTAINER_CLUSTER}"
    parameters="${parameters} EnvAwsServiceNamespace=${AWS_SERVICE_NAMESPACE}"
    parameters="${parameters} EnvDockerAwsAccount=${ENV_DOCKER_AWS_ACCOUNT_ID}"
    parameters="${parameters} EnvDockerAwsRegion=${ENV_DOCKER_AWS_REGION}"

    # debug
    ${logger} command "environment" "${environment:branch}"
    ${logger} command "branch" "${branch}"
    ${logger} command "currentBranch" "${currentBranch}"
    ${logger} command "aws :: credentials :: account id" "${AWS_DEPLOY_ACCOUNT_ID}"
    ${logger} command "aws :: credentials :: access key" "***${AWS_DEPLOY_ACCESS_KEY: -3}"
    ${logger} command "aws :: credentials :: secret key" "***${AWS_DEPLOY_SECRET_KEY: -3}"
    ${logger} command "aws :: credentials :: region" "${AWS_DEPLOY_REGION}"
    ${logger} command "aws :: ecr :: account id" "${ENV_DOCKER_AWS_ACCOUNT_ID}"
    ${logger} command "aws :: ecr :: region" "${ENV_DOCKER_AWS_REGION}"
    ${logger} command "aws :: cloud formation :: stack name" "${AWS_CLOUDFORMATION_STACK_NAME}"
    ${logger} command "aws :: cloud formation :: template" "${AWS_CLOUDFORMATION_TEMPLATE}"
    ${logger} command "aws :: cloud formation :: parameters" "${parameters}"

    # deploy
    ${logger} command "aws cloudformation deploy \
        --template-file ${AWS_CLOUDFORMATION_TEMPLATE} \
        --stack-name ${AWS_CLOUDFORMATION_STACK_NAME} \
        --no-fail-on-empty-changeset \
        --region ${AWS_DEPLOY_REGION} \
        --parameter-overrides $parameters"

    aws cloudformation deploy \
        --template-file ${AWS_CLOUDFORMATION_TEMPLATE} \
        --stack-name ${AWS_CLOUDFORMATION_STACK_NAME} \
        --no-fail-on-empty-changeset \
        --region ${AWS_DEPLOY_REGION} \
        --parameter-overrides $parameters

        # check if everything is ok
        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot deploy on amazon cloud formation. Events:"
            aws cloudformation describe-stack-events  --region ${AWS_DEPLOY_REGION} --stack-name ${AWS_CLOUDFORMATION_STACK_NAME}
            exit 1
        fi

    # bye!
    ${logger} success "Cloud formation updated correctly."
}
