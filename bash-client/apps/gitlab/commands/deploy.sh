#!/usr/bin/env bash

# variable for exit function
function_result=""

deploy() {

	# precheck if this is a gitlab job
	${me} gitlab check
	if [ ! $? = 0 ]; then exit $?; fi

	# keep args
	local args=$@

	# environment variables (sometimes exists or not, depends on the end-user)
	local env_host=$(${me} strings trim ${ENV_SERVER_HOST})
	local env_keypath=$(${me} strings trim ${ENV_SERVER_KEY})
	local env_username=$(${me} strings trim ${ENV_SERVER_USERNAME})
	local env_working_dir=$(${me} strings trim ${ENV_SERVER_WORKING_DIR})
	local env_project_type=$(${me} strings trim ${ENV_PROJECT_TYPE})
	local env_bucket=$(${me} strings trim ${ENV_WEBSITE_BUCKET})

	# always CI_PROJECT_NAME
	local application=$(${me} strings trim ${CI_PROJECT_NAME})

	# optional args
	local keypath=$(${me} strings argument keypath $args)
	local host=$(${me} strings argument host $args)
	local environment=$(${me} strings argument environment $args)
	local username=$(${me} strings argument username $args)
	local type=$(${me} strings argument type $args)
	local branch=$(${me} strings argument branch $args)
	local strategy=$(${me} strings argument strategy $args)
	local working_dir=$(${me} strings argument working_dir $args)
	local tag=$(${me} strings argument tag $args)
	local bucket=$(${me} strings argument bucket $args)

	# precheck project_type because if it is a bucket project, we should not check SSH connection
	precheckArgument "${type}" "${env_project_type}" "--type"
	type="${function_result}"

	# precheck working_dir
	precheckTag "${tag}"
	tag="${function_result}"

	# precheck environment
	precheckEnvironmentArgument "${environment}"
	environment="${function_result}"

	# precheck working_dir
	precheckArgument "${working_dir}" "${env_working_dir}" "--working_dir"
	working_dir="${function_result}"

	# Hey! We should stop here if this is a bucket project
	if [ "${type}" == "s3website" ] || [ "${type}" == "nextjswebsite" ]; then

		# precheck bucket
		precheckArgument "${bucket}" "${env_bucket}" "--bucket"
		bucket="${function_result}"

		${logger} title "Deploying application..."
		${logger} note "application > name: ${application}"
		${logger} note "application > environment: ${environment}"
		${logger} note "application > type: ${type}"
		${logger} note "application > tag: ${tag}"
		${logger} note "application > bucket: ${bucket}"
		${logger} note "application > working_dir: ${working_dir}"

		
		${me} server deploy --application="${application}" --environment="${environment}" --type="${type}" --working_dir="${working_dir}" --bucket="${bucket}" --tag="${tag}" --strategy="${strategy}"

		exit

	fi

	# precheck ssh: keyfile
	precheckArgument "${keypath}" "${env_keypath}" "--keypath"
	keypath="${function_result}"

	if [ ! -f "${keypath}" ]; then
		${logger} error "${keypath} does not exists."
		exit 1
	fi

	# precheck ssh: hostname
	precheckArgument "${host}" "${env_host}" "--host"
	host="${function_result}"

	# precheck ssh: username
	precheckArgument "${username}" "${env_username}" "--username"
	username="${function_result}"

	# go!

	# change permissions
	sudo chmod 600 ${keypath}

	# create the commands
	local commands="steplix update; steplix server deploy --application=${application} --environment=${environment} --type=${type} --working_dir=${working_dir}"

	if [ ! -z "${tag}" ]; then
 		commands="${commands} --tag=${tag}"
	fi

	if [ ! -z "${strategy}" ]; then
 		commands="${commands} --strategy=${strategy}"
	fi

	if [ ! -z "${branch}" ]; then
 		commands="${commands} --branch=${branch}"
	fi

	# log
	${logger} title "Deploying application..."
	${logger} note "application > name: ${application}"
	${logger} note "application > environment: ${environment}"
	${logger} note "application > type: ${type}"
	${logger} note "application > tag: ${tag}"
	${logger} note "ssh > host: ${host}"
	${logger} note "ssh > username: ${username}"
	${logger} note "ssh > keypath: ${keypath}"
	${logger} note "ssh > working_dir: ${working_dir}"

	# run!
	ssh -i ${keypath} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${username}@${host} "${commands}"

}

precheckEnvironmentArgument() {

	local arg_item=$1

	# by default, the argument
	function_result=${arg_item}

	if [ -z "${arg_item}" ]; then

		# get current branch
		local branch="${CI_COMMIT_REF_NAME}"

		# set new environment based on branch
		if [ "${branch}" == "master" ]; then
			function_result="p"
		elif [ "${branch}" == "staging" ]; then
			function_result="s"
		elif [ "${branch}" == "develop" ]; then
			function_result="t"
		elif [ "${branch}" == "testing" ]; then
			function_result="t"
		else
			${logger} error "--environment is necessary."
			tooltip
			exit 1
		fi

	fi
}

precheckArgument() {

	local arg_item=$1
	local env_item=$2
	local option_name=$3

	# by default, the argument
	function_result=${arg_item}

	# check
	if [ -z "${arg_item}" ]; then

		# precheck environment variable
		if [ -z "${env_item}" ]; then
			${logger} error "${option_name} is necessary."
			tooltip
			exit 1
		else
			function_result=${env_item}
		fi

	fi
}

precheckTag() {

	local branch="${CI_COMMIT_REF_NAME}"
	local tag=$1

	# default
	function_result=""


	if [ -z "${tag}" ] && ([ "${branch}" == "master" ] || [ "${branch}" == "staging" ]); then

		# get the tag list
		git fetch --tags > /dev/null 2>&1

		# get latest version
		local latestVersion="$(git describe --tags $(git rev-list --tags --max-count=1))"

		if [ -z "${latestVersion}" ]; then

			# cannot depoy nonexistent tags
			${logger} error "This project does not have any tag yet. You cannot deploy from branch master without a tag."
			exit 1

		fi

		# well done!
		tag="${latestVersion}"


	elif [ ! -z "${tag}" ] && ([ "${branch}" == "master" ] || [ "${branch}" == "staging" ]); then

		# get the tag list
		git fetch --tags > /dev/null 2>&1

		# check if tag exists
		local version=$(git tag --list | grep "${tag}")

		if [ -z "${version}" ]; then

			# cannot depoy nonexistent tags
			${logger} error "The given tag ${tag} does not exists."
			exit 1

		fi

	fi

	# finally, did tag exists?
	if [ ! -z "${tag}" ]; then

		# create the url
		function_result="s3://steplix-applications/${CI_PROJECT_PATH}/${tag}.tar.gz"

	fi

}

tooltip() {
	${logger} command "host" "is mandatory. Expected --host as argument or ENV_SERVER_HOST as environment variable."
	${logger} command "keypath" "is mandatory. Expected --keypath as argument or ENV_SERVER_KEY as environment variable."
	${logger} command "username" "is mandatory. Expected --username as argument or ENV_SERVER_USERNAME as environment variable."
	${logger} command "working_dir" "is mandatory. Expected --working_dir as argument or ENV_SERVER_WORKING_DIR as environment variable."
	${logger} command "type" "is mandatory. Expected --type as argument or ENV_PROJECT_TYPE as environment variable."
	${logger} command "environment" "is mandatory. Expected --environment as argument or we will default to testing, staging and production values based on the branch name (develop, staging or master only)."
	${logger} command "tag" "is optional. Expected --tag as argument. This functionality will only work for branch master."
}
