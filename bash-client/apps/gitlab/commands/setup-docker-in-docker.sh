#!/usr/bin/env bash

setup-docker-in-docker() {

    # precheck if this is a gitlab job
    ${me} gitlab check
    if [ ! $? = 0 ]; then exit $?; fi

    # variables
    local args=$@
    local verbose=$(${me} strings argument verbose $args)

    # precheck if docker is installed
    docker -v > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then
        ${logger} error "docker is not installed."
        exit 1

    fi

	# go!
	${logger} note "Install dependencies for solve Docker in Docker"

    if [ "${verbose}" == "true" ] || [ "${verbose}" == "1" ]; then
        setup-docker-in-docker-install-dependencies
    else
        setup-docker-in-docker-install-dependencies > /dev/null 2>&1
    fi

	${logger} success "dependencies for docker in docker installed."

}

setup-docker-in-docker-install-dependencies() {
    apk add --update --no-cache binutils-gold build-base curl file g++ gcc git less mandoc libstdc++ libffi-dev libc-dev linux-headers libxml2-dev libxslt-dev libgcrypt-dev make netcat-openbsd nodejs npm openssl pkgconfig postgresql-dev python3 py3-pip jq tzdata yarn
    pip3 install --upgrade pip
    curl -sL https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub
    curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.31-r0/glibc-2.31-r0.apk
    curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.31-r0/glibc-bin-2.31-r0.apk
    apk add --no-cache glibc-2.31-r0.apk glibc-bin-2.31-r0.apk
    curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip
    unzip awscliv2.zip
    aws/install
    rm -rf awscliv2.zip aws /usr/local/aws-cli/v2/*/dist/aws_completer /usr/local/aws-cli/v2/*/dist/awscli/data/ac.index /usr/local/aws-cli/v2/*/dist/awscli/examples
    rm glibc-2.31-r0.apk
    rm glibc-bin-2.31-r0.apk
}
