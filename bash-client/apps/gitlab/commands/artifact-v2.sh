#!/usr/bin/env bash

artifact-v2() {

    # precheck if this is a gitlab job
    ${me} gitlab check
    if [ ! $? = 0 ]; then exit $?; fi

    # keep args
    local args=$@

    # retrieve args
    local locked=$(${me} strings argument locked $args)

    # environment variables (sometimes exists or not, depends on the end-user)
    local env_project_type=$(${me} strings trim ${ENV_PROJECT_TYPE})
    local env_apk_path=$(${me} strings trim ${ENV_APK_PATH})
    
    # variables
    local version="${CI_COMMIT_TAG}"
    local folderpath="${CI_PROJECT_DIR}"
    local branch="${CI_COMMIT_REF_NAME}"
    local s3folderpath="${CI_PROJECT_PATH}"
    local bucket="s3://steplix-applications"

    # precheck: project type must exists
    if [ -z "${env_project_type}" ]; then
        ${logger} error "ENV_PROJECT_TYPE not defined."
        ${logger} note "add ENV_PROJECT_TYPE variable to Gitlab CI Settings"
        exit 1
    fi

    # precheck: android project without apk
    if [ "${env_project_type}" == "android" ]; then

        if [ -z "${env_apk_path}" ]; then
            ${logger} error "ENV_APK_PATH not defined."
            ${logger} note "add ENV_APK_PATH variable to Gitlab CI Settings"
            exit 1
        fi

        if [ ! -f "${env_apk_path}" ]; then
            ${logger} error "${ENV_APK_PATH} file does not exists."
            exit 1
        fi

    fi

    # precheck: s3folderpath must start with /
    if [[ "${s3folderpath}" != /* ]]; then
        s3folderpath="/${s3folderpath}"
    fi

    # precheck: s3folderpath must end with /
    if [[ "${s3folderpath}" != */ ]]; then
        s3folderpath="${s3folderpath}/"
    fi

    # precheck if amazon is already installed
    aws --version > /dev/null 2>&1

    # check if everything is ok
    if [ ! $? -eq 0 ]; then
        ${logger} error "aws it is not installed."
        ${logger} note "try to install aws with the following command: steplix install amazon"
        exit 1
    fi

    # precheck if amazon can access to S3
    # aws s3 ls > /dev/null 2>&1
    aws s3 ls > /dev/null 2>&1

    # check if everything is ok
    if [ ! $? -eq 0 ]; then
        ${logger} error "aws does not have permissions to interact with S3."
        ${logger} note "try to setup your aws account with the following command: steplix setup amazon"
        exit 1
    fi


    # precheck if amazon can access to the steplix bucket
    aws s3 ls "${bucket}" > /dev/null 2>&1

    # check if everything is ok
    if [ ! $? -eq 0 ]; then
        ${logger} error "${bucket} does not exists."
        exit 1
    fi

    # move to folderpath
    cd ${folderpath} > /dev/null 2>&1

    # check if everything is ok
    if [ ! $? -eq 0 ]; then
        ${logger} error "${folderpath} does not exists"
        exit 1
    fi

    # check if this is a .git project or not
    git status > /dev/null 2>&1

    # check if everything is ok
    if [ ! $? -eq 0 ]; then
        ${logger} error "${folderpath} does not have a .git repository"
        exit 1
    fi

    # default to 0.0.1 if there is not tags
    if [ -z "${version}" ]; then
        version="$(git describe --tags $(git rev-list --tags --max-count=1))"
    fi

    # create the tag file
    echo "${version}" > "TAG"

    # check if package.json exists, in that case use npm to write de package.json
    if [ -f "package.json" ]; then

        # Prevents throwing an error when npm version is used to set the new version to the same value as the current version
        npm config set allow-same-version true

        # a version bump but no tag or a new commit
        npm config set git-tag-version false

        # just in case, re-assign the current version
        npm --no-git-tag-version version "$version" > /dev/null 2>&1

        # increment the new version. This will write in package.json
        npm --no-git-tag-version version patch > /dev/null 2>&1

    fi

    # check if package.json exists, in that case use npm to compile the modules
    if [ -f "package.json" ]; then

        if [ "${env_project_type}" == "reactjs" ]; then
            compileReactProject
            exit 0

        elif [ "${env_project_type}" == "ghost" ]; then
            compileGhostProject "${locked}"
            exit 0

        elif [ "${env_project_type}" == "nextjs" ] || [ "${env_project_type}" == "docker-nextjs" ]; then
            compileNextProject
            exit 0

        elif [ "${env_project_type}" == "nextjswebsite" ]; then
            compileNextWebsiteProject
            exit 0

        else
            compileNodeProject
        fi

    fi

    # create the artifact
    local localfilepath="${folderpath}/${version}.tar.gz"

    # archive based on the project type
    if [ "${env_project_type}" == "reactjs" ]; then

        localfilepath="${folderpath}/../${version}.tar.gz"
        ${logger} note "executing: tar -czf ${localfilepath} ."
        tar --exclude="./.git" --exclude="./.gitignore" -czf "${localfilepath}" .

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: tar -czvf ../${lastCommit}.tar.gz . "
            exit 1
        fi

    else
        git archive --format=tar.gz HEAD > "${localfilepath}"

        # check if everything is ok
        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: git archive --format=tar.gz HEAD > \"${localfilepath}\""
            exit 1
        fi
    fi


    # move the source_code tag to S3
    ${me} amazon copy-file-to-s3 --localfilepath="${localfilepath}" --s3folderpath="${bucket}${s3folderpath}" --yyyymmddhhiiss="false"

    # android apk case:
    if [ "${env_project_type}" == "android" ]; then
        
        # rename the apk
        local directory=$(dirname "${env_apk_path}")
        local apk_path="${directory}/${version}.apk"

        mv "${env_apk_path}" "${apk_path}"

        # move the apk to S3
        ${me} amazon copy-file-to-s3 --localfilepath="${apk_path}" --s3folderpath="${bucket}${s3folderpath}" --yyyymmddhhiiss="false"
    fi

}


createReactDevelopmentTag() {

    local bucket=$1
    local folderpath=$(pwd)
    local lastCommit=$(git log --format="%H" -n 1)
    local localfilepath="${folderpath}/../${lastCommit}.tar.gz"

        # precheck: lastCommit exists?
        if [ -z "${lastCommit}" ]; then
            ${logger} error "cannot execute: git log --format=\"%H\" -n 1"
            exit 1
        fi

    # create the tag file
    echo "${lastCommit}" > "TAG"

    # compile
    compileReactProject

    # create the artifact
    ${logger} note "executing: tar -czf ../${lastCommit}.tar.gz ."

    tar --exclude="./.git" --exclude="./.gitignore" -czf "../${lastCommit}.tar.gz" .

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: tar -czvf ../${lastCommit}.tar.gz . "
            exit 1
        fi

    # mv to s3
    ${me} amazon copy-file-to-s3 --localfilepath="${localfilepath}" --s3folderpath="${bucket}${s3folderpath}" --yyyymmddhhiiss="false"

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: ${me} amazon copy-file-to-s3 --localfilepath=${localfilepath} --s3folderpath=${bucket}${s3folderpath} --yyyymmddhhiiss=false"
            exit 1
        fi

}

compileReactProject() {
    # install dependencies
    installReactDependencies

    # make build folder
    buildReactProject

    # remove node_modules (ask nico again in 1 month from May. 7th)
    # rm -rf node_modules
}

installReactDependencies() {
    ${logger} note "executing: npm ci"

    npm ci > /dev/null 2>&1

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: npm ci"
            exit 1
        fi
}

buildReactProject() {
    ${logger} note "executing: npm run build"

    # npm run build > /dev/null 2>&1
    npm run build

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: npm run build"
            exit 1
        fi
}

compileReactProject() {
    # install dependencies
    installReactDependencies

    # make build folder
    buildReactProject

    # remove node_modules (ask nico again in 1 month from May. 7th)
    # rm -rf node_modules
}

# ghost
createGhostDevelopmentTag() {

    local bucket=$1
    local folderpath=$(pwd)
    local lastCommit=$(git log --format="%H" -n 1)
    local localfilepath="${folderpath}/../${lastCommit}.tar.gz"

        # precheck: lastCommit exists?
        if [ -z "${lastCommit}" ]; then
            ${logger} error "cannot execute: git log --format=\"%H\" -n 1"
            exit 1
        fi

    # create the tag file
    echo "${lastCommit}" > "TAG"

    # compile
    compileGhostProject

    # copy necesary files
    ${logger} note "coping: config*.json"

    find . -maxdepth 0 -name "config*.json" -exec cp '{}' ./.build/release \;

    # create the artifact
    ${logger} note "executing: tar -czf ../${lastCommit}.tar.gz ."

    tar --exclude="./.git" --exclude="./.gitignore" -czf "../${lastCommit}.tar.gz" .build/release/

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: tar -czvf ../${lastCommit}.tar.gz . "
            exit 1
        fi

    # mv to s3
    ${me} amazon copy-file-to-s3 --localfilepath="${localfilepath}" --s3folderpath="${bucket}${s3folderpath}" --yyyymmddhhiiss="false"

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: ${me} amazon copy-file-to-s3 --localfilepath=${localfilepath} --s3folderpath=${bucket}${s3folderpath} --yyyymmddhhiiss=false"
            exit 1
        fi

}

compileGhostProject() {
    # install dependencies
    installGhostDependencies "${1}"

    # make build folder
    buildGhostProject

    # remove node_modules (ask nico again in 1 month from May. 7th)
    # rm -rf node_modules
}

installGhostDependencies() {
    ${logger} note "executing: yarn install"

    local locked=$1

    if [ "${locked}" == "true" ]; then
        yarn install --frozen-lockfile

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: yarn install --frozen-lockfile"
            exit 1
        fi
    else
        yarn install

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: yarn install"
            exit 1
        fi
    fi
}

buildGhostProject() {
    ${logger} note "executing: npm run release"

    APP_ENV=$APP_ENV npm run release > /dev/null 2>&1

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: npm run release"
            exit 1
        fi
}

# docker-nextjs
createNextDevelopmentTag() {

    local bucket=$1
    local folderpath=$(pwd)
    local lastCommit=$(git log --format="%H" -n 1)
    local localfilepath="${folderpath}/../${lastCommit}.tar.gz"

        # precheck: lastCommit exists?
        if [ -z "${lastCommit}" ]; then
            ${logger} error "cannot execute: git log --format=\"%H\" -n 1"
            exit 1
        fi

    # create the tag file
    echo "${lastCommit}" > "TAG"

    # compile
    compileNextProject

    # create the artifact
    ${logger} note "executing: tar -czf ../${lastCommit}.tar.gz ."

    tar --exclude="./.git" --exclude="./.gitignore" -czf "../${lastCommit}.tar.gz" ./

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: tar -czvf ../${lastCommit}.tar.gz . "
            exit 1
        fi

    # mv to s3
    ${me} amazon copy-file-to-s3 --localfilepath="${localfilepath}" --s3folderpath="${bucket}${s3folderpath}" --yyyymmddhhiiss="false"

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: ${me} amazon copy-file-to-s3 --localfilepath=${localfilepath} --s3folderpath=${bucket}${s3folderpath} --yyyymmddhhiiss=false"
            exit 1
        fi

}

compileNextProject() {
    # install dependencies
    installNextDependencies

    # make build folder
    buildNextProject

    # remove node_modules (ask nico again in 1 month from May. 7th)
    # rm -rf node_modules
}

installNextDependencies() {
    ${logger} note "executing: npm install"

    npm install > /dev/null 2>&1

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: npm install"
            exit 1
        fi
}

buildNextProject() {
    ${logger} note "executing: npm run build"

    # npm run build > /dev/null 2>&1
    npm run build

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: npm run build"
            exit 1
        fi
}

compileNextWebsiteProject() {
    # install dependencies
    installNextDependencies

    # make build folder
    buildNextWebsiteProject

    # remove node_modules (ask nico again in 1 month from May. 7th)
    # rm -rf node_modules
}

buildNextWebsiteProject() {
    ${logger} note "executing: npm run release"

    # npm run release > /dev/null 2>&1
    npm run release

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: npm run release"
            exit 1
        fi
}

compileNodeProject() {
    # install dependencies
    installNodeDependencies

    # make build folder
    # buildNodeProject

    # remove node_modules (ask nico again in 1 month from May. 7th)
    # rm -rf node_modules
}

installNodeDependencies() {
    ${logger} note "executing: npm install"

    if [ -f "package-lock.json" ]; then
        npm ci > /dev/null 2>&1

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute (clean install): npm ci"
            exit 1
        fi

    else
        npm i > /dev/null 2>&1

        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot execute: npm install"
            exit 1
        fi
    fi

}
