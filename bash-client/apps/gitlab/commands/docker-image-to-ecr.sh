#!/usr/bin/env bash

# It works only with environment variables
#		branch: 
#			develop					- nothing happens
#			production				- nothing happens
#			testing		generates	image:sdr4d1
#									image:latest
#			lab		generates	image:sdr4d1
#									image:latest
#			staging		generates	image:0.0.13
#									image:latest


docker-image-to-ecr() {


	# 01. GITLAB 	precheck if this is a gitlab job
	${me} gitlab check
	if [ ! $? = 0 ]; then exit 1; fi

	# keep args
	local args=$@

	# 02. ARGS 		mandatory environment variables
	local FORCE_TAG=$(${me} strings bool-argument forcetag $args)
	local FORCE_BRANCH=$(${me} strings bool-argument forcebranch $args)
	local ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${ENV_DOCKER_AWS_ACCOUNT_ID})
	local ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION})
	local ENV_DOCKER_IMAGE_NAME=$(${me} strings trim ${ENV_DOCKER_IMAGE_NAME})
	local ENV_AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY})
	local ENV_AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY})
	local ENV_BRANCH=$(${me} strings trim ${CI_COMMIT_REF_NAME})
	local ENV_SHORT_COMMIT_ID=$(${me} strings trim ${CI_COMMIT_SHORT_SHA})
	local TAG="${ENV_SHORT_COMMIT_ID}"

	#
	# Nico Molina :: We keep this variable, to avoid backwards compatibility errors. But please use "FORCE_TAG"
	local FORCE_TAG_VERSION=$(${me} strings bool-argument force-tag-version $args)
	#
	#

		# args check
		if [ ! -z "${ENV_AWS_ECR_NAME}" ]; then
	        ENV_DOCKER_IMAGE_NAME=$(${me} strings trim ${ENV_AWS_ECR_NAME})
		fi
		if [ -z "${ENV_DOCKER_IMAGE_NAME}" ]; then
	        ENV_DOCKER_IMAGE_NAME=$(${me} strings trim ${CI_PROJECT_PATH_SLUG})
		fi

		if [ "${ENV_BRANCH}" == "testing" ]; then
            if [ ! -z "${ENV_DOCKER_AWS_REGION_TESTING}" ]; then
                ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_TESTING})
            fi
        elif [ "${ENV_BRANCH}" == "staging" ]; then
            if [ ! -z "${ENV_DOCKER_AWS_REGION_STAGING}" ]; then
                ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_STAGING})
            fi
            if [ ! -z "${ENV_DOCKER_AWS_ACCOUNT_ID_STAGING}" ]; then
                ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${ENV_DOCKER_AWS_ACCOUNT_ID_STAGING})
            fi
            if [ ! -z "${ENV_AWS_DEPLOY_ACCESS_KEY_STAGING}" ]; then
                ENV_AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${ENV_AWS_DEPLOY_ACCESS_KEY_STAGING})
            fi
            if [ ! -z "${ENV_AWS_DEPLOY_SECRET_KEY_STAGING}" ]; then
                ENV_AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${ENV_AWS_DEPLOY_SECRET_KEY_STAGING})
            fi
        elif [ "${ENV_BRANCH}" == "lab" ]; then
            if [ ! -z "${ENV_DOCKER_AWS_REGION_LAB}" ]; then
                ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_LAB})
            fi
        fi
	

    # 03. BRANCH 	check if we are trying to create an image from testing or staging
    if [ "${ENV_BRANCH}" != "testing" ] && [ "${ENV_BRANCH}" != "staging" ] && [ "${ENV_BRANCH}" != "lab" ] && [ "${FORCE_BRANCH}" != "true" ]; then
        ${logger} error "We will not create the image from the current branch (${ENV_BRANCH}). We create images only from lab, testing or staging branch."
        exit 1
    fi


    # 04. AMAZON 	setup amazon for deployment
	${me} setup amazon --region=${ENV_DOCKER_AWS_REGION} --access_key=${ENV_AWS_DEPLOY_ACCESS_KEY} --secret_key=${ENV_AWS_DEPLOY_SECRET_KEY} > /dev/null 2>&1

	    # check if everything is ok
	    if [ ! $? -eq 0 ]; then
	        ${logger} error "cannot setup amazon."
	        ${logger} note "try to install aws with the following command: steplix install amazon"
	        exit 1
	    fi


	# 05. TAGS 		get the latest tag if staging
    if [ "${ENV_BRANCH}" == "staging" ] || [ "${FORCE_TAG_VERSION}" == "true" ] || [ "${FORCE_TAG}" == "true" ]; then

	    # get the full tag list
	    git fetch --tags

	    # get the latest tag (latestVersion)
	    local latestVersion="$(git describe --tags $(git rev-list --tags --max-count=1))"

	    # default to 0.0.1 if there is not tags
	    if [ -z "${latestVersion}" ]; then
	        latestVersion="0.0.0"
	    fi

	    # count the number of "." occurrences
	    local occurrences=$(awk -F"." '{print NF-1}' <<< "${latestVersion}")

	    # check if everything is ok
	    if [ "${occurrences}" != "2" ]; then
	        ${logger} error "${latestVersion} seems to be an invalid tag version (expected mayor.minor.patch like 0.0.15)"
	        exit 1
	    fi

	    # bye
	    TAG="${latestVersion}"

	    # As you notice, we did not increment the tag.
	    # This is because we delegate that task to TAG job.
        
    fi

    # 06. AWS ECR 		Deploy in Amazon Elastic Container Registry (ECR)
    docker build --network host -t ${ENV_DOCKER_IMAGE_NAME}:${TAG} .

	    # check if everything is ok
	    if [ ! $? -eq 0 ]; then
	        ${logger} error "cannot build the image ${ENV_DOCKER_IMAGE_NAME}:${TAG}"
	        exit 1
	    fi

    aws ecr get-login-password --region ${ENV_DOCKER_AWS_REGION} | docker login --username AWS --password-stdin ${ENV_DOCKER_AWS_ACCOUNT_ID}.dkr.ecr.${ENV_DOCKER_AWS_REGION}.amazonaws.com

	    # check if everything is ok
	    if [ ! $? -eq 0 ]; then
	        ${logger} error "cannot authenticate docker to amazon ECR: ${ENV_DOCKER_AWS_ACCOUNT_ID}.dkr.ecr.${ENV_DOCKER_AWS_REGION}.amazonaws.com"
	        exit 1
	    fi

	docker tag "${ENV_DOCKER_IMAGE_NAME}:${TAG}" "${ENV_DOCKER_AWS_ACCOUNT_ID}.dkr.ecr.${ENV_DOCKER_AWS_REGION}.amazonaws.com/${ENV_DOCKER_IMAGE_NAME}:${TAG}"

	    # check if everything is ok
	    if [ ! $? -eq 0 ]; then
	        ${logger} error "cannot tag the image ${ENV_DOCKER_IMAGE_NAME}:${TAG}"
	        exit 1
	    fi

    docker push "${ENV_DOCKER_AWS_ACCOUNT_ID}.dkr.ecr.${ENV_DOCKER_AWS_REGION}.amazonaws.com/${ENV_DOCKER_IMAGE_NAME}:${TAG}"

	    # check if everything is ok
	    if [ ! $? -eq 0 ]; then
	        ${logger} error "cannot push the image: ${ENV_DOCKER_AWS_ACCOUNT_ID}.dkr.ecr.${ENV_DOCKER_AWS_REGION}.amazonaws.com/${ENV_DOCKER_IMAGE_NAME}:${TAG}"
	        exit 1
	    fi

    # 07. SAVE TAG
    echo ${TAG} > tag

    # bye!
	${logger} success "docker image ${ENV_DOCKER_IMAGE_NAME}:${TAG} generated and pushed to AWS ECR."
}
