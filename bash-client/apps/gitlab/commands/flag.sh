#!/usr/bin/env bash

flag() {

	# precheck CI environment variables
	local cienv="${CI_PROJECT_NAME}"
	if [ -z "$cienv" ]; then
		${logger} error "cannot see Gitlab CI environment variables."
		exit 1
	fi

	# keep args
	local status=$1
	local filepath="/${CI_PROJECT_NAME}.flag"		# do not change, or, also change it in gitlab/slack.sh

	# trim
	status=$(echo $status | sed 's/ *$//g')

	# check mandatory
	if [ -z ${status} ]; then
		status="init"								# do not change, or, also change it in gitlab/slack.sh
	fi

	# write into flag
	echo ${status} > ${filepath}

	# bye
	${logger} success "flag saved at ${filepath}"
}
