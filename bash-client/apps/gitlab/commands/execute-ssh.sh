#!/usr/bin/env bash

execute-ssh() {

	# keep args
	local args=$@
	
	# mandatory args
	local keypath=""
	local ip=""
	local commands=""

	# optional args defaulting to CI environment variables
	local username=""

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${@//--/|}"

	for element in "${my_array[@]}"; do

		# get the keypath
		if [[ $element == *"keypath="* ]]; then
			keypath=${element//keypath=/}
		fi

		# get the ip
		if [[ $element == *"ip="* ]]; then
			ip=${element//ip=/}
		fi

		# get the commands
		if [[ $element == *"commands="* ]]; then
			commands=${element//commands=/}
		fi

		# get the username
		if [[ $element == *"username="* ]]; then
			username=${element//username=/}
		fi

	done

	# precheck mandatory fields
	if [ -z "$keypath" ] || [ -z "$ip" ] || [ -z "$commands" ]; then
		help
		exit 1
	fi

	# precheck optional fields
	if [ -z "$username" ]; then
	       username="ubuntu"
	fi

	# precheck key exists
	if [ ! -f ${keypath} ]; then
		${logger} error "${keypath} does not exists."
		exit 1
	fi

	# remove spaces
	username=$(echo $username | sed -e 's/^[[:space:]]*//')
	ip=$(echo $ip | sed -e 's/^[[:space:]]*//')


	# go!
	sudo chmod 600 ${keypath}

	${logger} note "ssh -i ${keypath} ${username}@${ip}"
	${logger} note "${commands}"

	ssh -i ${keypath} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${username}@${ip} ${commands}

	${logger} success "remote command executed."

}
