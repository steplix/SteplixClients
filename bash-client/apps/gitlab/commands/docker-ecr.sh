#!/usr/bin/env bash

docker-ecr() {

	# precheck if this is a gitlab job
	${me} gitlab check
	if [ ! $? = 0 ]; then exit $?; fi

	# keep args
	local args=$@

	# optional args defaulting to CI environment variables
	local aws_account_id=$(${me} strings argument aws-account-id $args)
	local aws_region=$(${me} strings argument aws-region $args)
	local image=$(${me} strings argument image $args)
	local tag=$(${me} strings argument tag $args)
	local latest=$(${me} strings argument latest $args)
	local only_latest=$(${me} strings argument only-latest $args)

	# default fields
	if [ -z "${aws_account_id}" ]; then
		aws_account_id=$(${me} strings trim ${AWS_DEPLOY_ACCOUNT_ID})
	fi
	if [ -z "${aws_account_id}" ]; then
		aws_account_id=$(${me} strings trim ${AWS_ACCOUNT_ID})
	fi
	if [ -z "${aws_region}" ]; then
		aws_region=$(${me} strings trim ${AWS_DEPLOY_REGION})
	fi
	if [ -z "${aws_region}" ]; then
		aws_region=$(${me} strings trim ${AWS_REGION})
	fi
	if [ -z "${image}" ]; then
		image=$(${me} strings trim ${CI_PROJECT_PATH_SLUG})
	else	
		image=$(echo $image | tr '[:upper:]' '[:lower:]')
	fi
	if [ -z "${tag}" ]; then
		tag="$(git describe --tags $(git rev-list --tags --max-count=1))"
		tag=$(${me} strings trim ${tag})
	fi
	if [ -z "${latest}" ]; then
		latest="false"
	fi
	if [ -z "${only_latest}" ]; then
		only_latest="false"
	fi

	# go!
	setup-aws-for-deploy

    if [ "${only_latest}" != "true" ]; then
		docker-ecr-tag "${aws_account_id}" "${aws_region}" "${image}" "${tag}"
	fi

    if [ "${latest}" == "true" ] || [ "${only_latest}" == "true" ]; then
    	docker-ecr-tag "${aws_account_id}" "${aws_region}" "${image}" "latest"
	fi

	${logger} success "docker image generated and pushed to AWS ECR correct."

}

docker-ecr-tag() {

	local aws_account_id=$1
	local aws_region=$2
	local image=$3
	local tag=$4

	# go!
	${logger} note "Generating docker image ${image}:${tag} and push this on AWS ECR"
    git fetch --tags

	${logger} note "Docker build image ${image}:${tag}"
    docker build --network host -t ${image}:${tag} .

	${logger} note "Authenticate docker with AWS ECR strategy on region ${aws_region}"
    aws ecr get-login-password --region ${aws_region} | docker login --username AWS --password-stdin ${aws_account_id}.dkr.ecr.${aws_region}.amazonaws.com

	${logger} note "Tagged ${image}:${tag} to ECR"
    docker tag "${image}:${tag}" "${aws_account_id}.dkr.ecr.${aws_region}.amazonaws.com/${image}:${tag}"

	${logger} note "Push ${image}:${tag} to ECR"
    docker push "${aws_account_id}.dkr.ecr.${aws_region}.amazonaws.com/${image}:${tag}"

}

setup-aws-for-deploy() {

    local env_region=$(${me} strings trim ${AWS_DEPLOY_REGION})
    local env_access_key=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY})
    local env_secret_key=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY})

    # default fields
    if [ -z "${env_region}" ]; then
        env_region=$(${me} strings trim ${AWS_REGION})
    fi

    if [ -z "${env_access_key}" ]; then
        env_access_key=$(${me} strings trim ${AWS_ACCESS_KEY})
    fi

    if [ -z "${env_secret_key}" ]; then
        env_secret_key=$(${me} strings trim ${AWS_SECRET_KEY})
    fi

    # configure other credentials
    ${me} setup amazon --region=${env_region} --access_key=${env_access_key} --secret_key=${env_secret_key} > /dev/null 2>&1
}
