#!/usr/bin/env bash

check() {

	# precheck if this is a gitlab job
	if [ -z "${CI_PROJECT_NAME}" ]; then
		${logger} error "cannot see Gitlab CI environment variables."
		${logger} note "this command it is only intended for gitlab."
		exit 1
	fi
}
