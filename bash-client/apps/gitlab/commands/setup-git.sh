#!/usr/bin/env bash

setup-git() {

	# precheck if this is a gitlab job
	${me} gitlab check
	if [ ! $? = 0 ]; then exit $?; fi

	# keep args
	local args=$@

	# environment variables (sometimes exists or not, depends on the end-user)
	local env_gitlab_access_token=$(${me} strings trim ${ENV_GITLAB_ACCESS_TOKEN})

	# optional fields
	local name=$(${me} strings argument name $args)
	local email=$(${me} strings argument email $args)
	local access_token=$(${me} strings argument access_token $args)

	# hello
	${logger} title "Setup git user, email and write permissions"

	# precheck optional fields
	if [ -z "${name}" ]; then
		name="Steplix"
	fi

	if [ -z "${email}" ]; then
		email="developer@steplix.com"
	fi

	if [ -z "${access_token}" ]; then
		if [ -z "${env_gitlab_access_token}" ]; then
			${logger} error "ENV_GITLAB_ACCESS_TOKEN is not defined in your gitlab account."
			exit 1
		else
			access_token="${env_gitlab_access_token}"
		fi
	fi

	# assign the username and emil
	${me} setup git-user --name=${name} --email=${email} > /dev/null 2>&1

	# assign project url if needed
	local url="https://gitlab-ci-token:${access_token}@gitlab.com/${CI_PROJECT_PATH}.git"

	cd ${CI_PROJECT_DIR} > /dev/null 2>&1
	git remote set-url origin ${url} > /dev/null 2>&1

	# bye
	${logger} success "git configuration done!"
}
