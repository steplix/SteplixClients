#!/usr/bin/env bash

setup-amazon() {

	# precheck if this is a gitlab job
	${me} gitlab check
	if [ ! $? = 0 ]; then exit $?; fi

	# variables
	local args=$@
	local ENV_BRANCH=$(${me} strings trim ${CI_COMMIT_REF_NAME})
	local env_region=$(${me} strings argument region $args)
	local env_access_key=$(${me} strings argument access_key $args)
	local env_secret_key=$(${me} strings argument secret_key $args)

	# default fields
	if [ -z "${env_region}" ]; then
		env_region=$(${me} strings trim ${AWS_REGION})

		if [ "${ENV_BRANCH}" == "testing" ]; then
            if [ ! -z "${AWS_REGION_TESTING}" ]; then
                env_region=$(${me} strings trim ${AWS_REGION_TESTING})
            fi
        elif [ "${ENV_BRANCH}" == "staging" ]; then
            if [ ! -z "${AWS_REGION_STAGING}" ]; then
                env_region=$(${me} strings trim ${AWS_REGION_STAGING})
            fi
        elif [ "${ENV_BRANCH}" == "lab" ]; then
            if [ ! -z "${AWS_REGION_LAB}" ]; then
                env_region=$(${me} strings trim ${AWS_REGION_LAB})
            fi
        fi
	fi

	if [ -z "${env_access_key}" ]; then
		env_access_key=$(${me} strings trim ${AWS_ACCESS_KEY})
	fi

	if [ -z "${env_secret_key}" ]; then
		env_secret_key=$(${me} strings trim ${AWS_SECRET_KEY})
	fi

	# precheck optional fields
	if [ -z "${env_region}" ]; then
		${logger} error "AWS_REGION environment variable not defined."
		exit 1
	fi

	if [ -z "${env_access_key}" ]; then
		${logger} error "AWS_ACCESS_KEY environment variable not defined."
		exit 1
	fi

	if [ -z "${env_secret_key}" ]; then
		${logger} error "AWS_SECRET_KEY environment variable not defined."
		exit 1
	fi

	# precheck amazon
	aws --version > /dev/null 2>&1

	if [ ! $? -eq 0 ]; then
		${me} install amazon
	fi

	# run!
	${me} setup amazon-by-set --region=${env_region} --access_key=${env_access_key} --secret_key=${env_secret_key} 
}
