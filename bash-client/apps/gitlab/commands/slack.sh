#!/usr/bin/env bash

slack() {

	# precheck if this is a gitlab job
	${me} gitlab check
	if [ ! $? = 0 ]; then exit $?; fi

	# keep args
	local args=$@

	# optional args defaulting to CI environment variables
	local channel=$(${me} strings argument channel $args)
	local environment=$(${me} strings argument environment $args)
	local result=$(${me} strings argument result $args)
	local job_url=$(${me} strings argument job_url $args)
	local task=$(${me} strings argument task $args)
	local project=$(${me} strings argument project $args)
	local template=$(${me} strings argument template $args)

	# defaults
	if [ -z "$environment" ]; then

		# get current branch
		local branch="${CI_COMMIT_REF_NAME}"
		local tag="${CI_COMMIT_TAG}"

		# default environment to develop
		environment="development"

		# set new environment based on tag or branch
		if [ -n "${tag}" ]; then
			environment="${tag}"
		elif [ "${branch}" == "master" ]; then
			environment="production"
		elif [ "${branch}" == "staging" ]; then
			environment="staging"
		elif [ "${branch}" == "develop" ]; then
			environment="testing"
		fi

		args="${args} --environment=${environment}"
	fi

	# defaults
	if [ -z "$result" ]; then
		result="not_flagged"

		# read from flag file if exists
		local filepath="/${CI_PROJECT_NAME}.flag"	# do not change, or, also change it in gitlab/flag.sh
		if [ -f "${filepath}" ]; then
			result=$(cat "${filepath}")
			if [ "${result}" = "init" ]; then
				result="error"
			fi
		fi
		args="${args} --result=${result}"
	fi

	# defaults
	if [ -z "$channel" ]; then
		args="${args} --channel=steplix-cli-notifications"
	fi

	# defaults
	if [ -z "$template" ]; then
		args="${args} --template=3"
	fi

	# defaults
	if [ -z "$job_url" ]; then
		args="${args} --job_url=${CI_JOB_URL}"
	fi

	# defaults
	if [ -z "$task" ]; then
		args="${args} --task=${CI_JOB_NAME}"
	fi

	# defaults
	if [ -z "$project" ]; then
		args="${args} --project=${CI_PROJECT_NAME}"
	fi

	# append 'to'
	args="${args} --to=${channel}"

	# go!
	${me} notifications slack ${args}

}
