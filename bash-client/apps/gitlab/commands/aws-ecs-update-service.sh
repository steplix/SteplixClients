#!/usr/bin/env bash

# It works only with environment variables
#       branch: 
#           develop                  - nothing happens
#           testing     update with  image:sdr4d1
#           staging     update with  image:0.0.13
#           production  update with  image:0.0.13


aws-ecs-update-service() {


    # 01. GITLAB    precheck if this is a gitlab job
    ${me} gitlab check
    if [ ! $? = 0 ]; then exit $?; fi


    # 02. ARGS      mandatory variables

    local args=$@

    local ENV_DOCKER_AWS_ACCOUNT_ID=$(${me} strings trim ${ENV_DOCKER_AWS_ACCOUNT_ID})
    local ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION})
    local ENV_DOCKER_IMAGE_NAME=$(${me} strings trim ${ENV_DOCKER_IMAGE_NAME})
    local ENV_AWS_DEPLOY_ACCESS_KEY=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY})
    local ENV_AWS_DEPLOY_SECRET_KEY=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY})
    local ENV_DOCKER_TASK_DEFINITION=$(${me} strings trim ${ENV_DOCKER_TASK_DEFINITION})
    local ENV_DOCKER_SERVICE=$(${me} strings trim ${ENV_DOCKER_SERVICE})
    local ENV_DOCKER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER})
    local ENV_BRANCH=$(${me} strings trim ${CI_COMMIT_REF_NAME})
    local BRANCH_NAME=$(${me} strings argument branch $args)
    local SUFFIX_NAME=$(${me} strings argument suffix $args)
    local TASK_DEFINITION_NAME=$(${me} strings argument task_definition_name $args)
    local SERVICE_NAME=$(${me} strings argument service_name $args)
    local CLUSTER_NAME=$(${me} strings argument cluster_name $args)
    local FARGATE_COMPATIBILITY=$(${me} strings argument fargate $args)
    local TAG=$(${me} strings argument tag $args)

        # args check
        if [ ! -z "${BRANCH_NAME}" ]; then
            ENV_BRANCH="${BRANCH_NAME}";
        fi

        if [ "${ENV_BRANCH}" == "testing" ]; then
            if [ ! -z "${ENV_DOCKER_CLUSTER_TESTING}" ]; then
                ENV_DOCKER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER_TESTING})
            fi
            if [ ! -z "${ENV_DOCKER_SERVICE_TESTING}" ]; then
                ENV_DOCKER_SERVICE=$(${me} strings trim ${ENV_DOCKER_SERVICE_TESTING})
            fi
            if [ ! -z "${ENV_DOCKER_TASK_DEFINITION_TESTING}" ]; then
                ENV_DOCKER_TASK_DEFINITION=$(${me} strings trim ${ENV_DOCKER_TASK_DEFINITION_TESTING})
            fi
            if [ ! -z "${ENV_DOCKER_AWS_REGION_TESTING}" ]; then
                ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_TESTING})
            fi
        elif [ "${ENV_BRANCH}" == "staging" ]; then
            if [ ! -z "${ENV_DOCKER_CLUSTER_STAGING}" ]; then
                ENV_DOCKER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER_STAGING})
            fi
            if [ ! -z "${ENV_DOCKER_SERVICE_STAGING}" ]; then
                ENV_DOCKER_SERVICE=$(${me} strings trim ${ENV_DOCKER_SERVICE_STAGING})
            fi
            if [ ! -z "${ENV_DOCKER_TASK_DEFINITION_STAGING}" ]; then
                ENV_DOCKER_TASK_DEFINITION=$(${me} strings trim ${ENV_DOCKER_TASK_DEFINITION_STAGING})
            fi
            if [ ! -z "${ENV_DOCKER_AWS_REGION_STAGING}" ]; then
                ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_STAGING})
            fi
        elif [ "${ENV_BRANCH}" == "production" ]; then
            if [ ! -z "${ENV_DOCKER_CLUSTER_PRODUCTION}" ]; then
                ENV_DOCKER_CLUSTER=$(${me} strings trim ${ENV_DOCKER_CLUSTER_PRODUCTION})
            fi
            if [ ! -z "${ENV_DOCKER_SERVICE_PRODUCTION}" ]; then
                ENV_DOCKER_SERVICE=$(${me} strings trim ${ENV_DOCKER_SERVICE_PRODUCTION})
            fi
            if [ ! -z "${ENV_DOCKER_TASK_DEFINITION_PRODUCTION}" ]; then
                ENV_DOCKER_TASK_DEFINITION=$(${me} strings trim ${ENV_DOCKER_TASK_DEFINITION_PRODUCTION})
            fi
            if [ ! -z "${ENV_DOCKER_AWS_REGION_PRODUCTION}" ]; then
                ENV_DOCKER_AWS_REGION=$(${me} strings trim ${ENV_DOCKER_AWS_REGION_PRODUCTION})
            fi
        fi

        if [ ! -z "${ENV_AWS_ECS_NAME}" ]; then
            ENV_DOCKER_TASK_DEFINITION=$(${me} strings trim ${ENV_AWS_ECS_NAME})
            ENV_DOCKER_SERVICE=$(${me} strings trim ${ENV_AWS_ECS_NAME})
        fi

        if [ ! -z "${TASK_DEFINITION_NAME}" ]; then
            ENV_DOCKER_TASK_DEFINITION=$(${me} strings trim ${TASK_DEFINITION_NAME})
        fi
        if [ ! -z "${SERVICE_NAME}" ]; then
            ENV_DOCKER_SERVICE=$(${me} strings trim ${SERVICE_NAME})
        fi
        if [ ! -z "${CLUSTER_NAME}" ]; then
            ENV_DOCKER_CLUSTER=$(${me} strings trim ${CLUSTER_NAME})
        fi

        if [ ! -z "${SUFFIX_NAME}" ]; then
            ENV_DOCKER_TASK_DEFINITION="${ENV_DOCKER_TASK_DEFINITION}${SUFFIX_NAME}"
            ENV_DOCKER_SERVICE="${ENV_DOCKER_SERVICE}${SUFFIX_NAME}"
        fi

        if [ "${ENV_AWS_ECS_TASK_NAME_ATTACH_ENV}" == "true" ]; then
            ENV_DOCKER_TASK_DEFINITION="${ENV_DOCKER_TASK_DEFINITION}-${ENV_BRANCH}"
        fi

        if [ ! -z "${ENV_AWS_ECR_NAME}" ]; then
            ENV_DOCKER_IMAGE_NAME=$(${me} strings trim ${ENV_AWS_ECR_NAME})
        fi
        if [ -z "${ENV_DOCKER_IMAGE_NAME}" ]; then
            ENV_DOCKER_IMAGE_NAME=$(${me} strings trim ${CI_PROJECT_PATH_SLUG})
        fi

        if [ -z "${ENV_DOCKER_CLUSTER}" ]; then
            ${logger} error "missing environment variable ENV_DOCKER_CLUSTER, ENV_DOCKER_CLUSTER_TESTING or ENV_DOCKER_CLUSTER_STAGING"
            exit 1
        fi

    # urls
    ENV_DOCKER_TASK_DEFINITION="arn:aws:ecs:${ENV_DOCKER_AWS_REGION}:${ENV_DOCKER_AWS_ACCOUNT_ID}:task-definition/${ENV_DOCKER_TASK_DEFINITION}"
    ENV_DOCKER_SERVICE="arn:aws:ecs:${ENV_DOCKER_AWS_REGION}:${ENV_DOCKER_AWS_ACCOUNT_ID}:service/${ENV_DOCKER_CLUSTER}/${ENV_DOCKER_SERVICE}"
    ENV_DOCKER_CLUSTER="arn:aws:ecs:${ENV_DOCKER_AWS_REGION}:${ENV_DOCKER_AWS_ACCOUNT_ID}:cluster/${ENV_DOCKER_CLUSTER}"

    local DOCKER_URL="${ENV_DOCKER_AWS_ACCOUNT_ID}.dkr.ecr.${ENV_DOCKER_AWS_REGION}.amazonaws.com/${ENV_DOCKER_IMAGE_NAME}:${TAG}"

    # 03. TAG       mandatory
    if [ -z "${TAG}" ]; then
        ${logger} error "missing --tag argument"
        exit 1
    fi

    # 04. BRANCH    check if we are trying to create an image from testing, staging or production
    if [ "${ENV_BRANCH}" != "testing" ] && [ "${ENV_BRANCH}" != "staging" ] && [ "${ENV_BRANCH}" != "production" ] && [ "${ENV_BRANCH}" != "master" ]; then
        ${logger} error "We will not create the image from the current branch (${ENV_BRANCH}). We create images only from testing, staging or production branch."
        exit 1
    fi

    # 05. AMAZON    setup amazon for deployment
    ${me} setup amazon-by-set --region=${ENV_DOCKER_AWS_REGION} --access_key=${ENV_AWS_DEPLOY_ACCESS_KEY} --secret_key=${ENV_AWS_DEPLOY_SECRET_KEY}

        # check if everything is ok
        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot setup amazon."
            ${logger} note "try to install aws with the following command: steplix install amazon"
            exit 1
        fi


    # 06. Hello
    ${logger} command "cluster" "${ENV_DOCKER_CLUSTER}"
    ${logger} command "service" "${ENV_DOCKER_SERVICE}"
    ${logger} command "task-definition" "${ENV_DOCKER_TASK_DEFINITION}"
    ${logger} command "docker-url" "${DOCKER_URL}"
    ${logger} command "tag" "${TAG}"


    # 07. TASK_DEFINITION   get the latest task definition
    local TASK_DEFINITION=""

    if [ -f task-definition-base.json ]; then

        ${logger} info "Use task definition file."
        TASK_DEFINITION=$(cat task-definition-base.json)

    else

        ${logger} info "Find task definition on ECS."
        echo "aws ecs describe-task-definition --task-definition $ENV_DOCKER_TASK_DEFINITION --region ${ENV_DOCKER_AWS_REGION}"
        TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition $ENV_DOCKER_TASK_DEFINITION --region ${ENV_DOCKER_AWS_REGION})
        echo "${TASK_DEFINITION}"

        # 08. CURATE            curate Task Definition
        TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq .taskDefinition)

        TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq "del(.status)")
        TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq "del(.revision)")
        TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq "del(.compatibilities)")
        TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq "del(.taskDefinitionArn)")
        TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq "del(.requiresAttributes)")
        TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq "del(.registeredAt)")
        TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq "del(.registeredBy)")

        echo "${TASK_DEFINITION}"

        if ! $(${me} json has --json="${TASK_DEFINITION}" --key='containerDefinitions[0].memory'); then
            TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq ".containerDefinitions[0].memory = 150")
        fi

        if [ ! -z "${FARGATE_COMPATIBILITY}" ] && [ "${FARGATE_COMPATIBILITY}" == "true" ]; then
            if ! $(${me} json has --json="${TASK_DEFINITION}" --key='requiresCompatibilities'); then
                TASK_DEFINITION=$(echo "${TASK_DEFINITION}" | jq '.requiresCompatibilities = ["FARGATE"]')
            fi
        fi
    fi

    # 09. JSON  write in tmp file the task definition
    echo $TASK_DEFINITION | jq '.' > task-definition.json

    # 10. JSON  change the docker tag
    tmp=$(mktemp)
    jq ".containerDefinitions[0].image = \"${DOCKER_URL}\"" task-definition.json > "$tmp" && mv "$tmp" task-definition.json


    # 11. TASK_DEFINITION   update task definition
    aws ecs register-task-definition --cli-input-json file://task-definition.json --region ${ENV_DOCKER_AWS_REGION}

        # check if everything is ok
        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot register task definition on ECS."
            ${logger} note "JSON:"
            cat task-definition.json
            exit 1
        fi


    # 12. SERVICE           Great! now we need to update the service with the new task definition
    aws ecs update-service --service $ENV_DOCKER_SERVICE --task-definition $ENV_DOCKER_TASK_DEFINITION --cluster $ENV_DOCKER_CLUSTER --region ${ENV_DOCKER_AWS_REGION}

        # check if everything is ok
        if [ ! $? -eq 0 ]; then
            ${logger} error "cannot update service on ECS."
            exit 1
        fi

    # bye!
    ${logger} success "task definition updated and service restarted with the new version ${TAG}."
}
