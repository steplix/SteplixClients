#!/usr/bin/env bash

setup-key() {

	# script variables
	local private_key="id_ed25519"
	local public_key="${private_key}.pub"
	local ssh_path="$HOME/.ssh"
	local privatefilepath="${ssh_path}/${private_key}"
	local knownhostspath="${ssh_path}/known_hosts"

	# mandatory fields
	local privatekey=""
	local knownhosts=""

	# hello
	${logger} title "Setup the ssh-key based on a given file"

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${@//--/|}"

	for element in "${my_array[@]}"; do

		# get the privatekey
		if [[ $element == *"privatekey="* ]]; then
			privatekey=${element//privatekey=/}
		fi

		# get the knownhosts
		if [[ $element == *"knownhosts="* ]]; then
			knownhosts=${element//knownhosts=/}
		fi

	done

	# trim
	privatekey=$(echo $privatekey | sed 's/ *$//g')
	knownhosts=$(echo $knownhosts | sed 's/ *$//g')

	# precheck mandatory fields
	if [ -z "$privatekey" ] || [ -z "$knownhosts" ]; then
		help
		exit 1
	fi

	# check if key exists
	if [ ! -f "${privatekey}" ]; then
		${logger} error "The ${privatekey} does not exists";
	    exit 1
	fi

	# check if key knownhosts
	if [ ! -f "${knownhosts}" ]; then
		${logger} error "The ${knownhosts} does not exists";
	    exit 1
	fi

	# create .ssh folder if not exists
	mkdir ${ssh_path} > /dev/null 2>&1

	# go
	mv ${privatekey} ${privatefilepath}
	if [ ! $? -eq 0 ]; then
		${logger} error "Cannot move ${privatekey} to ${privatefilepath}";
		return
	fi

	mv ${knownhosts} ${knownhostspath}
	if [ ! $? -eq 0 ]; then
		${logger} error "Cannot move ${knownhosts} to ${knownhostspath}";
		return
	fi

	# change mod
	chmod 600 ${privatefilepath}

	# success!
	${logger} success "Your private was created at ${privatefilepath}";
	${logger} success "knownhosts file was created at ${knownhostspath}";
}