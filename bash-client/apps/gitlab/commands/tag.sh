#!/usr/bin/env bash

tag() {

    # precheck if this is a gitlab job
    ${me} gitlab check
    if [ ! $? -eq 0 ]; then exit 1; fi

    ${me} git tag --levelpattern="${CI_COMMIT_MESSAGE}" --folderpath="${CI_PROJECT_DIR}" --branch="${CI_COMMIT_REF_NAME}"
    if [ ! $? -eq 0 ]; then exit 1; fi
}
