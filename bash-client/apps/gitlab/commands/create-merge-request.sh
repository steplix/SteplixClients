#!/usr/bin/env bash

create-merge-request() {

    # precheck if this is a gitlab job
    ${me} gitlab check
    if [ ! $? = 0 ]; then exit $?; fi

    # keep args
    local args=$@

    # retrieve args
    local projectId=$(${me} strings trim ${CI_PROJECT_ID})
    local currentBranch=$(${me} strings trim ${CI_COMMIT_BRANCH})
    local token=$(${me} strings trim ${STEPLIX_GITLAB_ACCESS_TOKEN})
    local host=$(${me} strings trim ${CI_PROJECT_URL})
    local branch=$(${me} strings argument branch $args)
    local target=$(${me} strings argument target $args)
    local failOnError=$(${me} strings bool-argument failonerror $args)

    # precheck optional args
    if [ -z "$branch" ]; then
        branch=${currentBranch}
    fi
    if [ -z "$token" ]; then
        token=$(${me} strings trim ${GITLAB_ACCESS_TOKEN})
    fi

    # precheck require args
    if [ -z "$target" ]; then
        ${logger} error "Please set target branch"
        if [ "${failOnError}" == "true" ]; then exit 1; else exit 0; fi
    fi
    if [ -z "$token" ]; then
        ${logger} error "Please set gitlab access token"
        if [ "${failOnError}" == "true" ]; then exit 1; else exit 0; fi
    fi

    # retrieve constants

    # Extract the host where the server is running, and add the URL to the APIs
    [[ $host =~ ^https?://[^/]+ ]] && host="${BASH_REMATCH[0]}/api/v4/projects/"

    local body="{
        \"id\": ${projectId},
        \"source_branch\": \"${branch}\",
        \"target_branch\": \"${target}\",
        \"title\": \"Merge ${branch} on ${target}\"
    }";


    # Require a list of all the merge request and take a look if there is already one with the same source branch
    local listmr=`curl --silent "${host}${projectId}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${STEPLIX_GITLAB_ACCESS_TOKEN}"`;
    local countBranchs=`echo ${listmr} | grep -o "\"source_branch\":\"${branch}\"" | wc -l`;

    # No MR found, let's create a new one
    if [ ${countBranchs} -eq "0" ]; then
        curl -X POST "${host}${projectId}/merge_requests" \
            --header "PRIVATE-TOKEN:${STEPLIX_GITLAB_ACCESS_TOKEN}" \
            --header "Content-Type: application/json" \
            --data "${body}";

        # check if everything is ok
        if [ ! $? -eq 0 ]; then
            if [ "${failOnError}" == "true" ]; then
                ${logger} error "cannot create merge request: from ${branch} to ${target}"
                exit 1
            fi
        fi

        ${logger} success "Opened a new merge request: from ${branch} to ${target}";
    fi
}
