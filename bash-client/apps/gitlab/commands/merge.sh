#!/usr/bin/env bash

merge() {

    # precheck if this is a gitlab job
    ${me} gitlab check
    if [ ! $? -eq 0 ]; then exit 1; fi

    # keep args
    local args=$@

    # retrieve args
    local folderpath=$(${me} strings argument folderpath $args)
    local message=$(${me} strings argument message $args)
    local branch=$(${me} strings argument branch $args)
    local origin=$(${me} strings argument origin $args)
    local url=$(${me} strings argument url $args)

    # precheck require args
    if [ -z "$origin" ]; then
        ${logger} error "Please, origin branch."
        exit 1
    fi

    # precheck optional args
    if [ -z "$folderpath" ]; then
		folderpath="${CI_PROJECT_DIR}"
    fi
    if [ -z "$branch" ]; then
		branch="${CI_COMMIT_REF_NAME}"
    fi
    if [ -z "$message" ]; then
		message="Pipeline - Auto merge ${branch} with ${origin}"
    fi
    if [ -z "$url" ]; then
		url="https://${STEPLIX_GITLAB_ACCESS_USER}:${STEPLIX_GITLAB_ACCESS_TOKEN}@${CI_REPOSITORY_URL#*@}"
    fi

    ${me} git merge --folderpath="${folderpath}" --branch="${branch}" --origin="${origin}" --message="${message}" --url="${url}"
    if [ ! $? -eq 0 ]; then exit 1; fi
}
