#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix pipeline application
# @author      	Steplix
# @description	useful commands for gitlab pipelines
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "artifact" "artifact-v2" "check" "deploy" "execute-ssh" "flag" "setup-docker-in-docker" "setup-amazon" "setup-git" "setup-key" "slack" "docker-ecr" "docker-image-to-ecr" "aws-ecs-update-service" "aws-cloudformation-deploy" "tag" "merge" "create-merge-request")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/artifact.sh"
source "${commands_path}/artifact-v2.sh"
source "${commands_path}/check.sh"
source "${commands_path}/deploy.sh"
source "${commands_path}/execute-ssh.sh"
source "${commands_path}/flag.sh"
source "${commands_path}/setup-docker-in-docker.sh"
source "${commands_path}/setup-amazon.sh"
source "${commands_path}/setup-git.sh"
source "${commands_path}/setup-key.sh"
source "${commands_path}/slack.sh"
source "${commands_path}/docker-ecr.sh"
source "${commands_path}/docker-image-to-ecr.sh"
source "${commands_path}/aws-ecs-update-service.sh"
source "${commands_path}/aws-cloudformation-deploy.sh"
source "${commands_path}/tag.sh"
source "${commands_path}/merge.sh"
source "${commands_path}/create-merge-request.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

