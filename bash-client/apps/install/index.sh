#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix install application
# @author      	Steplix
# @description	Install everything you need
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "common" "upgrade" "gitlab" "amazon" "amazon2" "apache" "composer-client" "php7" "java8" "mysql-client" "nodejs" "pm2-client" "docker-client")


# this script helpers
# ==========================================================
aptget() {

	local app=$1

	# go!
	sudo apt-get --quiet install --yes ${app} > /dev/null 2>&1

	# check if everything is ok
	if [ $? -eq 0 ]; then
	    ${logger} success "${app} installed."
	else
	    ${logger} error "cannot install ${app}."
	fi

}



# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/amazon.sh"
source "${commands_path}/amazon2.sh"
source "${commands_path}/gitlab.sh"
source "${commands_path}/upgrade.sh"
source "${commands_path}/common.sh"
source "${commands_path}/apache.sh"
source "${commands_path}/php7.sh"
source "${commands_path}/composer-client.sh"
source "${commands_path}/java8.sh"
source "${commands_path}/mysql-client.sh"
source "${commands_path}/nodejs.sh"
source "${commands_path}/docker-client.sh"
source "${commands_path}/android.sh"
source "${commands_path}/pm2-client.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

