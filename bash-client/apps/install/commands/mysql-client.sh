#!/usr/bin/env bash

mysql-client() {

	local root_password=""

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${@//--/|}"

	for element in "${my_array[@]}"; do

		# get the root password
		if [[ $element == *"root_password="* ]]; then
			root_password=${element//root_password=/}
		fi

	done

	# check
	if [ -z "${root_password}" ]
	then
		help
		exit 1
	fi
	

	# go!
	${logger} title "Installing MySQL v5.7 ..."

	sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password ${root_password}"
	sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password ${root_password}"
	sudo apt-get -y -qq install mysql-server > /dev/null 2>&1

	# check
	mysql --version > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(mysql --version)
	    ${logger} success "mysql installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install mysql."
	fi
}