#!/usr/bin/env bash

composer-client() {

	# hello
	${logger} title "Installing Composer..."

	# precheck: php is needed
	php -v > /dev/null 2>&1

	if [ ! $? -eq 0 ]; then
	    ${logger} error "PHP is required. Abort."
	    exit 1
	fi

	# go
	rm -rf ~/composer/ 
	mkdir ~/composer/
	cd ~/composer/


	# download
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php composer-setup.php
	php -r "unlink('composer-setup.php');"

	# make it globally
	sudo mv composer.phar /usr/local/bin/composer

	# clean
	cd
	rm -rf ~/composer/

	# check if everything is ok
	composer --version

	if [ $? -eq 0 ]; then
		version=$(composer --version)
	    ${logger} success "composer installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install composer."
	fi

}