#!/usr/bin/env bash

docker-client() {

	# 1. hello
	${logger} title "Installing Docker..."

	# 2. Add Docker’s official GPG key:
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -  > /dev/null 2>&1

	# 3. Use the following command to set up the stable repository.
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable" > /dev/null 2>&1

	# 4. Update the apt package index, and install the latest version of Docker Engine and containerd
	sudo apt-get update > /dev/null 2>&1
	sudo apt-get -y install docker-ce docker-ce-cli containerd.io > /dev/null 2>&1

	# 5. check
	docker -v > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(docker -v)
	    ${logger} success "docker installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install docker."
	    ${logger} note "execute: steplix install common and try again."
	fi

}