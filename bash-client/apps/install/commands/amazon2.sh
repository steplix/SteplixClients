#!/usr/bin/env bash

amazon2() {
	
	${logger} title "Installing Amazon AWS Client..."

	# Go
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" > /dev/null 2>&1
	unzip awscliv2.zip > /dev/null 2>&1
	sudo ./aws/install > /dev/null 2>&1

	# check if everything is ok
	aws --version > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(aws --version)
	    ${logger} success "aws installed."
	    ${logger} note ${version}
	    ${logger} note "\033[0maws command works with \033[0;33menv variables\033[0m. You can also setup an aws profile with \033[0;33msteplix setup amazon\033[0m"
	else
	    ${logger} error "cannot install aws."
	fi

}