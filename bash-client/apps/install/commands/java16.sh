#!/usr/bin/env bash

java11() {

	# hello
	${logger} title "Installing JAVA v16.x ..."

	# update
	sudo add-apt-repository -y ppa:openjdk-r/ppa > /dev/null 2>&1
	sudo apt-get update -y > /dev/null 2>&1

	# go
	sudo apt-get install -y openjdk-16-jdk-headless > /dev/null 2>&1

	# check if everything is ok
	java -version > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(java -version 2>&1 | head -n 1 | awk -F '"' '{print $2}')
	    ${logger} success "java installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install java."
	fi
}