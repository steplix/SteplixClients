#!/usr/bin/env bash

apache() {

	${logger} title "Installing Apache2..."

	# Go
	sudo apt-get install -y apache2 > /dev/null 2>&1

	# check if everything is ok
	apache2 -version > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(apache2 -version | head -n 1)
	    ${logger} success "apache2 installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install apache2."
	fi
}