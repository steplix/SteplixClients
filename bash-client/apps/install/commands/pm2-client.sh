#!/usr/bin/env bash

pm2-client() {


	# check if npm is already installed
	npm -v > /dev/null 2>&1

	# check if everything is ok
	if [ ! $? -eq 0 ]; then
	    ${logger} error "node and npm are required. Please, install node and try again."
	fi

	# Hello
	${logger} title "Installing PM2..."

	# Go
	sudo npm install pm2@latest -g > /dev/null 2>&1

	# check if everything is ok
	pm2 --version > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(pm2 --version)
	    ${logger} success "pm2 installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install pm2."
	fi

}