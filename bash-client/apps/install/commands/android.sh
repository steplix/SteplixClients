#!/usr/bin/env bash

android() {
	
	# 1. Check Dependencies
	wget --version > /dev/null 2>&1
	if [ ! $? -eq 0 ]; then
		${logger} error "wget is neccesary. Please run steplix install common and try again."
		exit 1
	fi

	unzip > /dev/null 2>&1
	if [ ! $? -eq 0 ]; then
		${logger} error "unzip is neccesary. Please run steplix install common and try again."
		exit 1
	fi

	sudo -V > /dev/null 2>&1
	if [ ! $? -eq 0 ]; then
		${logger} error "sudo is neccesary. Please run steplix install common and try again."
		exit 1
	fi

	# 2. Say Hello
	${logger} title "Installing Android sdkmanager v29.0.3 ..."
	${logger} note "please wait, we are downloading 1GB."

	# 3. Create the folder at home
	cd
	rm -rf androidsdk
	mkdir androidsdk
	cd androidsdk

	# 4. Download & Unzip
	wget https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip > /dev/null 2>&1
	unzip sdk-tools-linux-3859397.zip > /dev/null 2>&1
	rm sdk-tools-linux-3859397.zip

	# 5. Export the PATH
	local android_directory=$(pwd)
	local string="export PATH=${android_directory}/tools:${android_directory}/tools/bin:$PATH"

	cd 											# move to home directory because we do not know the username
	local filename="$(pwd)/.bashrc"				# the .bashrc
	echo ${string} >> ${filename}				# append to the end

	# 6. Install the dependencies
	local me=$(whoami)

	yes | sudo -u ${me} "${android_directory}/tools/bin/sdkmanager" --licenses > /dev/null 2>&1
	sudo -u ${me} "${android_directory}/tools/bin/sdkmanager" "build-tools;29.0.3" > /dev/null 2>&1
	sudo -u ${me} "${android_directory}/tools/bin/sdkmanager" "platforms;android-29" > /dev/null 2>&1


	# check if everything is ok
	eval "${android_directory}/tools/bin/sdkmanager" --list > /dev/null 2>&1

	if [ $? -eq 0 ]; then
	    ${logger} success "android sdkmanager installed."
	    ${logger} note "You have to ${YELLOW}$ source ${filename}${NOCOLOR}"
	else
	    ${logger} error "cannot install android sdkmanager ."
	fi

}