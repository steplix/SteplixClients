#!/usr/bin/env bash

gitlab() {

	${logger} title "Installing Gitlab Runner..."
	${logger} note "downloading ~400MB"

	# # Go!
	curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb > /dev/null 2>&1
	sudo dpkg -i gitlab-runner_amd64.deb > /dev/null 2>&1
	rm gitlab-runner_amd64.deb > /dev/null 2>&1

	# check if everything is ok
	gitlab-runner --version

	if [ $? -eq 0 ]; then
		version=$(gitlab-runner --version 2>&1 | head -n 1 | awk -F ' ' '{print $2}')
	    ${logger} success "gitlab-runner installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install gitlab-runner."
	fi
}