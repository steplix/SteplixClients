#!/usr/bin/env bash

upgrade() {

	${logger} title "Updating and Upgrading ubuntu package repositories..."
    
    sudo apt-get -y update > /dev/null 2>&1
    ${logger} success "update done."

    sudo apt-get -y upgrade > /dev/null 2>&1
    ${logger} success "upgrade done."
}