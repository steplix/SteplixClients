#!/usr/bin/env bash

php7() {

	upgrade 	# upgadre ubuntu

	# hello
	${logger} title "Installing PHP v7.3..."

	# add the PHP 7.3 repository
	sudo apt-get install -y python-software-properties > /dev/null 2>&1
	sudo add-apt-repository -y ppa:ondrej/php > /dev/null 2>&1
	sudo apt-get -y update > /dev/null 2>&1
	
	# install PHP
	sudo apt-get install -y --allow-unauthenticated php7.3 > /dev/null 2>&1

	# check if everything is ok
	php -v > /dev/null 2>&1

	if [ ! $? -eq 0 ]; then
	    ${logger} error "cannot install PHP v7.3"
	    exit 1
	fi

	# install dependencies
	sudo apt-get install -y php7.3-common > /dev/null 2>&1
	sudo apt-get install -y php7.3-opcache > /dev/null 2>&1
	sudo apt-get install -y php7.3-cli > /dev/null 2>&1
	sudo apt-get install -y php7.3-gd > /dev/null 2>&1
	sudo apt-get install -y php7.3-curl > /dev/null 2>&1
	sudo apt-get install -y php7.3-mysql > /dev/null 2>&1
	sudo apt-get install -y php7.3-zip > /dev/null 2>&1
	sudo apt-get install -y php7.3-xml > /dev/null 2>&1
	sudo apt-get install -y php7.3-mbstring > /dev/null 2>&1

	# bye
	version=$(php -v 2>&1 | head -n 1)
    ${logger} success "php installed."
    ${logger} note ${version}

}