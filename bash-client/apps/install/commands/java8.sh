#!/usr/bin/env bash

java8() {

	# hello
	${logger} title "Installing JAVA v8.x ..."

	# go
	sudo apt-get install -y openjdk-8-jdk-headless > /dev/null 2>&1

	# check if everything is ok
	java -version > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(java -version 2>&1 | head -n 1 | awk -F '"' '{print $2}')
	    ${logger} success "java installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install java."
	fi
}