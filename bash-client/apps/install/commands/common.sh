#!/usr/bin/env bash

common() {

	${logger} title "Installing common applications..."

	# this is a special case (docker) when there is no sudo installed =/
	sudo -V  > /dev/null 2>&1

	# check if everything is ok
	if [ ! $? -eq 0 ]; then
		apt-get install sudo > /dev/null 2>&1
	    ${logger} success "sudo installed."
	fi

	# Go as usual.
	aptget "wget"
	aptget "curl"
	aptget "vim"
	aptget "sed"
	aptget "tar"
	aptget "unzip"
	aptget "jq"
	aptget "lib32stdc++6"
	aptget "lib32z1"
	aptget "apt-transport-https"
	aptget "ca-certificates"
	aptget "software-properties-common"

	# update
	sudo apt-get update > /dev/null 2>&1
}