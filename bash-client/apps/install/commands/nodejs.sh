#!/usr/bin/env bash

#!/usr/bin/env bash

nodejs() {

	# hello
	${logger} title "Installing Node v10.x and NPM ..."

	# update repositories
	curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash - >  /dev/null 2>&1

	sudo apt-get install -y nodejs > /dev/null 2>&1

	# check
	node -v > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(node -v)
	    ${logger} success "nodejs installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install node."
	fi

	# check
	npm -v > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		version=$(npm -v)
	    ${logger} success "npm installed."
	    ${logger} note ${version}
	else
	    ${logger} error "cannot install npm."
	fi

}