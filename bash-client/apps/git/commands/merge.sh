#!/usr/bin/env bash

merge() {

    # vars
    local tag=""
    local level=""
    local folderpath=""
    local currentpath=$(pwd)
    local result=0

    # keep args
    local args=$@

    # retrieve args
    local folderpath=$(${me} strings argument folderpath $args)
    local message=$(${me} strings argument message $args)
    local username=$(${me} strings argument username $args)
    local email=$(${me} strings argument email $args)
    local branch=$(${me} strings argument branch $args)
    local origin=$(${me} strings argument origin $args)
    local url=$(${me} strings argument url $args)

    # precheck require args
    if [ -z "$origin" ]; then
        ${logger} error "Please, origin branch."
        exit 1
    fi

    # precheck optional args
    if [ -z "$username" ]; then
        username="Developer Steplix"
    fi
    if [ -z "$email" ]; then
        email="developer@steplix.com"
    fi
    if [ -z "$branch" ]; then
        branch="$(git rev-parse --symbolic-full-name --abbrev-ref HEAD)"
    fi
    if [ "$branch" == "$origin" ]; then
        ${logger} error "Please, set an origin branch other than the destination branch."
        exit 1
    fi
    if [ -z "$message" ]; then
        message="Auto merge ${branch} with ${origin}"
    fi

    # hello
    ${logger} title "Merge branches ${branch} with ${origin}"

    # check if folder if a git repository
    ${me} git check ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # move to folderpath if neccessary
    if [ -z "${folderpath}" ]; then
        folderpath=$(pwd)
    fi

    cd ${folderpath} > /dev/null 2>&1

    # configure repository
    git config --global user.name "$username"
    git config --global user.email "$email"

    # cache current git url
    local currentUrl="$(git config --get remote.origin.url)"

    # assign project url if needed
    if [ ! -z "${url}" ]; then
        git remote set-url origin ${url}

        if [ ! $? -eq 0 ]; then
            ${logger} error "${url} does not set on git origin url."
            cd ${currentpath}
            exit 1
        fi
    fi

    # fetch all
    git fetch --all
    if [ ! $? -eq 0 ]; then
        ${logger} error "Can't fetch branches"
        cd ${currentpath}
        exit 1
    fi

    # assign project branch if needed
    if [ ! -z "${url}" ]; then
        git checkout ${branch}

        if [ ! $? -eq 0 ]; then
            ${logger} error "${branch} does not checkout."
            cd ${currentpath}
            exit 1
        fi
    fi

    # merge branches
    git merge origin/${origin} -m "${message}"
    if [ ! $? -eq 0 ]; then
        ${logger} error "Can't merge branches '${branch}' with '${origin}'"
        cd ${currentpath}
        exit 1
    fi

    # push merge
    git push -f origin ${branch}
    if [ ! $? -eq 0 ]; then
        ${logger} error "Can't push to git branch '${branch}'"
        cd ${currentpath}
        exit 1
    fi

    # restore previous url
    git remote set-url origin "$currentUrl"

    # go to previous directory path (pwd)
    cd ${currentpath}

    # bye
    ${logger} success "Merged."

}
