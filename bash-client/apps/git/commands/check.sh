#!/usr/bin/env bash

check() {

    # vars
    local folderpath=""
    local currentpath=$(pwd)

    # keep args
    local args=$@

    # retrieve args
    local folderpath=$(${me} strings argument folderpath $args)

    # move to folderpath if neccessary
    if [ ! -z "${folderpath}" ]; then
        
        cd ${folderpath} > /dev/null 2>&1

        if [ ! $? -eq 0 ]; then
            ${logger} error "${folderpath} does not exists."
            ${logger} warning "Please, use absolute path."
            exit 1
        fi
    else
        folderpath=$(pwd)
    fi

    # check if this is a .git project or not
    git status > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "${folderpath} does not have a .git repository."
        cd ${currentpath}
        exit 1
    fi

    # bye
    cd ${currentpath}

}
