#!/usr/bin/env bash

update() {

    # vars
    local branch=""
    local folderpath=""
    local stash=false
    local checkout=false
    local stashed=false
    local currentpath=$(pwd)
    local result=0

    # keep args
    local args=$@

    # get args replacing '--' with '|'
    IFS='|' read -ra my_array <<< "${@//--/|}"

    for element in "${my_array[@]}"; do

        # get the folderpath
        if [[ $element == *"folderpath="* ]]; then
            folderpath=${element//folderpath=/}
        fi

        # get the branch
        if [[ $element == *"branch="* ]]; then
            branch=${element//branch=/}
        fi

        # get the stash
        if [[ $element == *"stash="* ]]; then
            stash=${element//stash=/}

            if [ "$stash" == "true" ] || [ "$stash" == "1" ]; then
                stash=true
            else
                stash=false
            fi
        fi

        # get the checkout
        if [[ $element == *"checkout="* ]]; then
            checkout=${element//checkout=/}

            if [ "$checkout" == "true" ] || [ "$checkout" == "1" ]; then
                checkout=true
            else
                checkout=false
            fi
        fi

    done

    # hello
    ${logger} title "Update repository"

    # check if folder if a git repository
    ${me} git check ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # move to folderpath if neccessary
    if [ -z "${folderpath}" ]; then
        folderpath=$(pwd)
    fi

    cd ${folderpath} > /dev/null 2>&1

    # check optional fields
    if [ -z "$branch" ]; then
        branch="$(git rev-parse --symbolic-full-name --abbrev-ref HEAD)"
    fi

    #
    # Check if need checkout
    #
    if [ $checkout == true ]; then

        git checkout . > /dev/null 2>&1

        if [ ! $? -eq 0 ]; then
            ${logger} error "Checkout changes failed."
            cd ${currentpath}
            exit $?
        fi

    #
    # Check if need stash
    #
    elif [ $stash == true ]; then

        # Check if has changes
        if [ -n "$(git status --porcelain)" ]; then

            git stash > /dev/null 2>&1
            stashed=true

            if [ ! $? -eq 0 ]; then
                ${logger} error "Stash changes failed."
                cd ${currentpath}
                exit $?
            fi
        fi
    fi

    #
    # Try to fetch and rebase all remote changes
    #
    git pull > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "Pull changes from remote failed."
        cd ${currentpath}
        exit $?
    fi

    #
    # Checkout branch to $branch
    #
    git checkout ${branch} > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "Checkout to branch ${branch} failed."
        cd ${currentpath}
        exit $?
    fi

    #
    # Try to fetch and rebase from origin branch
    #
    git pull origin ${branch} > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "Pull from origin branch ${branch} failed."
        cd ${currentpath}
        exit $?
    fi

    #
    # Check if need stash
    #
    if [ $stash == true ] && [ $stashed == true ]; then

        git stash pop > /dev/null 2>&1

        if [ ! $? -eq 0 ]; then
            ${logger} error "Unstash changes failed."
            cd ${currentpath}
            exit $?
        fi
    fi

    cd ${currentpath}

    # bye
    ${logger} success "Repository updated."

}
