#!/usr/bin/env bash

push() {

    # vars
    local tag=""
    local level=""
    local folderpath=""
    local currentpath=$(pwd)
    local result=0

    # keep args
    local args=$@

    # retrieve args
    local folderpath=$(${me} strings argument folderpath $args)
    local message=$(${me} strings argument message $args)
    local username=$(${me} strings argument username $args)
    local email=$(${me} strings argument email $args)
    local branch=$(${me} strings argument branch $args)
    local url=$(${me} strings argument url $args)

    # precheck optional fields
    if [ -z "$username" ]; then
        username="Developer Steplix"
    fi
    if [ -z "$email" ]; then
        email="developer@steplix.com"
    fi
    if [ -z "$branch" ]; then
        branch="$(git rev-parse --symbolic-full-name --abbrev-ref HEAD)"
    fi
    if [ -z "$message" ]; then
        ${logger} error "Please, commit message."
        exit 1
    fi

    # hello
    ${logger} title "Pushing repository"

    # check if folder if a git repository
    ${me} git check ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # move to folderpath if neccessary
    if [ -z "${folderpath}" ]; then
        folderpath=$(pwd)
    fi

    cd ${folderpath} > /dev/null 2>&1

    # configure repository
    git config --global user.name "$username"
    git config --global user.email "$email"

    # cache current git url
    local currentUrl="$(git config --get remote.origin.url)"

    # assign project url if needed
    if [ ! -z "${url}" ]; then
        git remote set-url origin ${url}

        if [ ! $? -eq 0 ]; then
            ${logger} error "${url} does not set on git origin url."
            cd ${currentpath}
            exit 1
        fi
    fi

    # assign project branch if needed
    if [ ! -z "${url}" ]; then
        git checkout ${branch}

        if [ ! $? -eq 0 ]; then
            ${logger} error "${branch} does not checkout."
            cd ${currentpath}
            exit 1
        fi
    fi

    # add everything to a new commit
    git add .

    git commit -m "$message"
    if [ ! $? -eq 0 ]; then
        ${logger} error "Can't commit to git with message '${message}'"
        cd ${currentpath}
        exit 1
    fi

    git push -f origin ${branch}
    if [ ! $? -eq 0 ]; then
        ${logger} error "Can't push to git branch '${branch}'"
        cd ${currentpath}
        exit 1
    fi

    # restore previous url
    git remote set-url origin "$currentUrl"

    # go to previous directory path (pwd)
    cd ${currentpath}

    # bye
    ${logger} success "Pushed."

}
