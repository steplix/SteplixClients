#!/usr/bin/env bash

tag() {

    # vars
    local tag=""
    local level=""
    local folderpath=""
    local levelpattern=""
    local currentpath=$(pwd)
    local result=0
    local rx=""

    # keep args
    local args=$@

    # retrieve args
    local folderpath=$(${me} strings argument folderpath $args)
    local levelpattern=$(${me} strings argument levelpattern $args)
    local level=$(${me} strings argument level $args)
    local tag=$(${me} strings argument tag $args)
    local url=$(${me} strings argument url $args)

    # precheck optional fields
    if [ -z "$level" ]; then
        if [ -z "$levelpattern" ]; then
            level="patch"
        else
            # check if level pattern has hash #major
            rx='^(#major)$'
            if [[ $levelpattern =~ $rx ]]; then
                level="major"
            else
                # check if level pattern has hash #minor
                rx='^(#minor)$'
                if [[ $levelpattern =~ $rx ]]; then
                    level="minor"
                else
                    level="patch"
                fi
            fi
        fi
    fi

    # hello
    ${logger} title "Tagging repository"

    # check if folder if a git repository
    ${me} git check ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # move to folderpath if neccessary
    if [ -z "${folderpath}" ]; then
        folderpath=$(pwd)
    fi

    cd ${folderpath} > /dev/null 2>&1

    # assign project url if needed
    if [ ! -z "${url}" ]; then
        git remote set-url origin ${url}
    fi

    # get the full tag list
    git fetch --tags

    # check optional fields
    if [ -z "$tag" ]; then

        # check if already have any tag
        if [ ! -z "$(git tag --list)" ]; then

            # take last tag version on list tags
            tag=$(git describe --tags $(git rev-list --tags --max-count=1))

            # increase tag version by level
            tag="$(${me} helpers semver-increase --version=${tag} --level=${level})"
        else
            # default tag version
            tag='0.0.1'
        fi
    fi

    # # make the new tag with a commit
    git tag ${tag} -m "v${tag}" -f
    if [ ! $? -eq 0 ]; then
        ${logger} error "Can't create tag '${tag}'"
        cd ${currentpath}
        exit 1
    fi

    # # push the tag
    git push origin --tags -f
    if [ ! $? -eq 0 ]; then
        ${logger} error "Can't push tag '${tag}'"
        cd ${currentpath}
        exit 1
    fi

    # go to previous directory path (pwd)
    cd ${currentpath}

    # bye
    ${logger} success "Tag created."

}
