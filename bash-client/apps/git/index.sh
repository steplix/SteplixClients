#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix pipeline application
# @author      	Steplix
# @description	useful commands for git
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "check" "tag" "update" "push" "merge")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/check.sh"
source "${commands_path}/tag.sh"
source "${commands_path}/update.sh"
source "${commands_path}/push.sh"
source "${commands_path}/merge.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

