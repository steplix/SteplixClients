#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix parent function class
# @author      	Steplix
# @description	Common functions to be loaded in every app
# ==========================================================

# Development or Production?
# ==========================================================
APP_PATH="${HOME}/.steplix"
PRODUCTION=true
me="/usr/local/bin/steplix"

if [ ! -f ${me} ] || [ "${PRODUCTION}" = false ]; then
    APP_PATH="/home/steplix/src/SteplixClients/bash-client"
    PRODUCTION=false
    me="${APP_PATH}/steplix"
fi

if [ -n "${CUSTOM_APP_PATH}" ] ; then
    APP_PATH="${CUSTOM_APP_PATH}"
    PRODUCTION=false
    me="${CUSTOM_APP_PATH}/steplix"
fi


# color variables
# ==========================================================
source "${APP_PATH}/apps/.base/colors.sh"
logger="${APP_PATH}/apps/logger/index.sh"
documentation="${APP_PATH}/apps/.documentation/index.sh"


# help functions
# ==========================================================
help() {

	local me=${0//\/index.sh/}
			me=`basename "$me"`

	${documentation} show "${me}.yml"

}

help_and_bye_with_error() {
	help
	exit 1
}


# arguments functions
# ==========================================================
check_min_arguments() {

	local passed=$1
	local min_required=1

	if [ ${passed} -lt ${min_required} ]
	then
		help_and_bye_with_error
	fi

}

check_command_available() {

	local command=$1
	local found=false

	# iterate over all available command
	for key in "${available_commands[@]}"
	do
		if [[ ${key} == ${command} ]]; then
			found=true
		fi
	done

	# did I found the command?
	if ! ${found}
	then
		help_and_bye_with_error
	fi
}

check_arguments() {

	local passed=$#
	local app=$1

	check_min_arguments ${passed}
	check_command_available ${app}
}

# execution function
# ==========================================================
execute() {

	local command=$1

	shift			# remove the command name from arguments list
	${command} $@	# execute the command with arguments

}

# to uppercase function
# ==========================================================
to_upper() {
	echo ${1^^}
}

# to uppercase the first letter function
# ==========================================================
to_title() {
	echo ${1^}
}