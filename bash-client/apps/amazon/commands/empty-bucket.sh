#!/usr/bin/env bash

empty-bucket() {

    # precheck amazon
    ${me} amazon check

    # variables
    local args=$@
    local bucket=$(${me} strings argument bucket $args)

    # mandatory
    if [ -z ${bucket} ]; then 
        ${logger} error "bucket argument is required."
        exit 1
    fi

    # precheck if folder path is a s3:// path
    if [[ "${bucket}" != s3://* ]]; then
        ${logger} error "${bucket} must start with s3://"
        exit 1
    fi

    # precheck if bucket exists
    aws s3 ls "${bucket}" > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then
        ${logger} error "${bucket} does not exists."
        exit 1
    fi

    # go go go
    ${logger} title "Empty bucket..."

    aws s3 rm "${bucket}" --recursive > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "command failed: aws s3 rm ${bucket} --recursive"
        exit 1
    fi

    ${logger} success "${bucket} is empty."
}