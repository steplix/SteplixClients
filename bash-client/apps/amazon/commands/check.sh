#!/usr/bin/env bash

check() {

    # precheck if aws is installed
    aws --version > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then
        ${logger} error "aws is not installed."
        exit 1
    fi

    # precheck if amazon can access to S3
    aws s3 ls > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then
        ${logger} error "aws does not have permissions to interact with S3."
        ${logger} note "try to setup your aws account with the following command: steplix setup amazon"
        exit 1
    fi

}
