#!/usr/bin/env bash

copy-file-to-s3() {

	# precheck amazon
    ${me} amazon check

	# variables
	local args=$@
	local localfilepath=$(${me} strings argument localfilepath $args)
	local s3folderpath=$(${me} strings argument s3folderpath $args)
	local yyyymmddhhiiss=$(${me} strings argument yyyymmddhhiiss $args)

	# mandatory
	if [ -z ${localfilepath} ]; then 
		${logger} error "localfilepath argument is required."
		exit 1
	fi

	# mandatory
	if [ -z ${s3folderpath} ]; then 
		${logger} error "s3folderpath argument is required."
		exit 1
	fi

	# optional
	if [ -z ${yyyymmddhhiiss} ]; then 
		yyyymmddhhiiss="true"
	fi

	# precheck localfile existency
	if [ ! -f "${localfilepath}" ]; then
		${logger} error "${localfilepath} does not exists."
		exit 1
	fi

	# append '/' to the s3 path if neccesary
	if [[ "${s3folderpath}" != */ ]]; then
	    s3folderpath="${s3folderpath}/"
	fi

	# precheck if folder path is a s3:// path
	if [[ "${s3folderpath}" != s3://* ]]; then
		${logger} error "${s3folderpath} must start with s3://"
		exit 1
	fi

	# filename
	local filename="$(basename -- ${localfilepath})"
	if [ ${yyyymmddhhiiss} == "true" ]; then
		yyyymmddhhiiss=$(date '+%Y%m%d%H%M%S')
		filename="${yyyymmddhhiiss}-${filename}"
	fi

	# go go go
	${logger} title "Uploading to S3..."

	local origin="${localfilepath}"
	local destination="${s3folderpath}${filename}"

	aws s3 cp "${origin}" "${destination}" > /dev/null 2>&1
	if [ ! $? -eq 0 ]; then
		${logger} error "error uploading to ${destination}"
		exit 1
	fi

	${logger} success "${destination} created."
}