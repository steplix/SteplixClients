#!/usr/bin/env bash

copy-folder-to-s3() {

    # precheck amazon
    ${me} amazon check

    # variables
    local args=$@
    local folder=$(${me} strings argument folder $args)
    local bucket=$(${me} strings argument bucket $args)

    # mandatory
    if [ -z ${folder} ]; then 
        ${logger} error "folder argument is required."
        exit 1
    fi

    # mandatory
    if [ -z ${bucket} ]; then 
        ${logger} error "bucket argument is required."
        exit 1
    fi

    # precheck folder existency
    if [ ! -d "${folder}" ]; then
        ${logger} error "${folder} does not exists."
        exit 1
    fi

    # precheck if folder path is a s3:// path
    if [[ "${bucket}" != s3://* ]]; then
        ${logger} error "${bucket} must start with s3://"
        exit 1
    fi

    # precheck if bucket exists
    aws s3 ls "${bucket}" > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then
        ${logger} error "${bucket} does not exists."
        exit 1
    fi

    # go go go
    ${logger} title "Uploading to S3..."

    local current_directory=$(pwd)
    
    cd "${folder}"
    aws s3 cp . "${bucket}" --recursive > /dev/null 2>&1

    local result=$?

    # AWS CLI Return Codes
    # These are the following return codes returned at the end of execution of a CLI command:
    #
    #  0 -- Command was successful. There were no errors thrown by either the CLI or by the service the request was made to.
    #  1 -- Limited to s3 commands, at least one or more s3 transfers failed for the command executed.
    #
    # For more information see https://docs.aws.amazon.com/cli/latest/topic/return-codes.html

    if [ ! $result -eq 0 ] && [ ! $result -eq 1 ]; then
        cd "${current_directory}"
        ${logger} error "error uploading ${folder} to ${bucket}"
        exit 1
    fi

    cd "${current_directory}"

    ${logger} success "${folder} successfully synchronized to ${bucket}."
}