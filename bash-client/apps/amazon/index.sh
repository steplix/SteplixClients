#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix amazon application
# @author      	Steplix
# @description	useful commands for amazon
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "check" "copy-file-to-s3" "copy-folder-to-s3" "empty-bucket")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/check.sh"
source "${commands_path}/copy-file-to-s3.sh"
source "${commands_path}/copy-folder-to-s3.sh"
source "${commands_path}/empty-bucket.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

