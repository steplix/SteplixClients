#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix documentation function class
# @author      	Steplix
# @description	Print a beautiful help
# ==========================================================

# this function read a .yml file and create a variable per each yml line.
parse_yaml() {

   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_-]*' fs=$(echo @|tr @ '\034')

   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
   	  gsub(/-/, "", $2)
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'

}

# display the .yml file in a beautiful format
show() {

	# Fix for error 'tput: No value for $TERM and no -T specified'.
	#   For more information see: https://forum.gitlab.com/t/tput-error-message-when-using-pipe-in-bash-scripts/11781
	if [ ! -z "${TERM}" ]; then
		export TERM=dumb
	fi

	# colors
	local BLACK=$(tput setaf 0)
	local RED=$(tput setaf 1)
	local GREEN=$(tput setaf 2)
	local YELLOW=$(tput setaf 3)
	local LIME_YELLOW=$(tput setaf 190)
	local POWDER_BLUE=$(tput setaf 153)
	local BLUE=$(tput setaf 4)
	local MAGENTA=$(tput setaf 5)
	local CYAN=$(tput setaf 6)
	local WHITE=$(tput setaf 7)
	local BRIGHT=$(tput bold)
	local NORMAL=$(tput sgr0)
	local BLINK=$(tput blink)
	local REVERSE=$(tput smso)
	local UNDERLINE=$(tput smul)

	# vars
	local folderpath=$(printf "%s/.documentation" $(dirname $(dirname $0))) 
	local ymlfile="${folderpath}/$1"
	local current_section=""

	# check if file exists
	if [ ! -f "${ymlfile}" ]; then
	    echo "${ymlfile} does not exist"
	    return
	fi

	# create yml_ variables, one by one
	eval $(parse_yaml "${ymlfile}" "yml_")	

	# hello
	printf "\n%s\n" "${CYAN}Documentation${NORMAL}"

	# read .yml line by line
	while IFS= read -r line
	do
		# is this a new line?
		if [ -z "${line}" ]; then
			continue
		fi

		# is this a new section? Must not start with <space>
		if [[ ! $line = \ * ]] ; then
			printf "\n‣ %s\n" "${CYAN}Available ${line}${NORMAL}"
		    #${logger} note "Available ${line}"
		    current_section=$(echo ${line} | sed -e 's/^[[:space:]]*//')	# trim
		else
			local prefix2=${line:0:2}
			local prefix4=${line:0:4}

			# is this a description por example? Must start with <space><space><space><space>
			if [[ "${prefix4}" = "    "  ]]; then
				continue
			else
				# Yey! this is a new command!

				# sanatize string because their contains `:` as sufix and `<space(s)>` as prefix.
				local section_sanatized=${current_section%?}
				local command_sanatized=$(echo ${line} | sed -e 's/^[[:space:]]*//')	# trim
				      command_sanatized=${command_sanatized%?}

				local command_for_dynamic_variable=$(sed -e "s/\-//g" <<< $command_sanatized)	# because we replaced '-' for ''

				# the keys
				local description_key="yml_${section_sanatized}_${command_for_dynamic_variable}_description"
				local example_key="yml_${section_sanatized}_${command_for_dynamic_variable}_example"

				# log
				printf '  %-22s%s%0s %s\n' "${YELLOW}${command_sanatized}" "${NORMAL}${!description_key}" "" "${GREEN}\$ ${!example_key}${NORMAL}"
		    fi
		fi
	done < "${ymlfile}"

	printf "\n"

}


# Go go go!
command=$1
shift			# remove the command name from arguments list
${command} $@	# execute the command with arguments