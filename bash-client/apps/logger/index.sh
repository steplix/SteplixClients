#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix logger application
# @author      	Steplix
# @description	Log with colors and icons
# ==========================================================

# color variables
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/colors.sh"
documentation="$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.documentation/index.sh"

# available commands
# ==========================================================
declare -a available_commands=("help" "title" "success" "info" "command" "warning" "error" "step" "note" "newline")


# colors functions
# ==========================================================

print() {
	echo -e "$@"
}

	title() {
		local string=$@
	    print "${LIGHT_CYAN}${string}${NOCOLOR}"
	}

	success() {
	    print "${GREEN}✔${NOCOLOR} $@"
	}

	info() {
		local key=$1
		shift # removes key from arguments
		local string=$@
	    print "${key}${YELLOW} ${string}${NOCOLOR}"
	}

	command() {
		local key=$1
		shift # removes key from arguments
		local string=$@
	    print "${YELLOW}  ${key}${NOCOLOR} ${string}"
	}

	error() {
		local string=$@
	    print "${RED}✖ ${string}${NOCOLOR}"
	}

	step() {
		local string=$@
	    echo -ne "${CYAN}•${NOCOLOR} ${string}${NOCOLOR}\r"
	}

	warning() {
		local string=$@
	    print "${YELLOW}➜${NOCOLOR} ${string}${NOCOLOR}"
	}

	note() {
		local string=$@
		print "‣ ${CYAN}${string}${NOCOLOR}"
	}

	newline() {
		echo ""
	}


# help functions
# ==========================================================
	help() {
		${documentation} show "logger.yml"
	}

help_and_bye_with_error() {
	help
	exit 1
}


# arguments functions
# ==========================================================
	check_min_arguments() {

		local passed=$1
		local min_required=1

		if [ ${passed} -lt ${min_required} ]
		then
			help_and_bye_with_error
		fi

	}

	check_command_available() {

		local command=$1
		local found=false

		# iterate over all available command
		for key in "${available_commands[@]}"
		do
		   if [[ ${key} == ${command} ]]; then
				found=true
		   fi
		done

		# did I found the command?
		if ! ${found}
		then
			help_and_bye_with_error
		fi
	}

check_arguments() {

	local passed=$#
	local app=$1

	check_min_arguments ${passed}
	check_command_available ${app}
}

# apps functions
# ==========================================================
execute() {

	local command=$1

	# remove the command name from arguments list
	shift

	# execute the command with arguments
	${command} $@

}

# Run!
# ==========================================================
check_arguments $@
execute $@

