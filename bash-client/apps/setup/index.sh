#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix step application
# @author      	Steplix
# @description	Setup amazon, utf8, git, etc.
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "git-ps1" "git-user" "git-aliases" "utf-8" "amazon" "amazon-by-set" "sync-date" "ssh-key" "signature" "docker-login")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/amazon.sh"
source "${commands_path}/amazon-by-set.sh"
source "${commands_path}/docker-login.sh"
source "${commands_path}/utf-8.sh"
source "${commands_path}/git-aliases.sh"
source "${commands_path}/git-user.sh"
source "${commands_path}/git-ps1.sh"
source "${commands_path}/sync-date.sh"
source "${commands_path}/ssh-key.sh"
source "${commands_path}/signature.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

