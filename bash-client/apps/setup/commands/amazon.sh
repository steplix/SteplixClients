#!/usr/bin/env bash

amazon() {


	local region=""
	local access_key=""
	local secret_key=""

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${@//--/|}"

	if [ ! $? -eq 0 ]; then
	    ${logger} error "error getting arguments in setup amazon"
	    exit 1
	fi

	for element in "${my_array[@]}"; do

		# get the region
		if [[ $element == *"region="* ]]; then
			region=${element//region=/}
		fi

		# get the access key
		if [[ $element == *"access_key="* ]]; then
			access_key=${element//access_key=/}
		fi

		# get the secret key
		if [[ $element == *"secret_key="* ]]; then
			secret_key=${element//secret_key=/}
		fi

	done

	# check
	if [ -z "${region}" ] || [ -z "${access_key}" ] || [ -z "${secret_key}" ]
	then
		help
		exit 1
	fi
	

	# go!
	${logger} title "Setup Amazon Profile"

	cd 	# move to home

	local foldername="$(pwd)/.aws"
	local config_file="${foldername}/config"
	local credentials_file="${foldername}/credentials"

	# make folder if not exists
	if [ ! -d "${foldername}" ]; then
		mkdir -p "${foldername}"
	fi

	# backup if neccesary
	if [ -f ${config_file} ]; then
		mv ${config_file} "${config_file}.bkp"
		${logger} note "${config_file} already exists. A backup has been placed at ${YELLOW}${config_file}.bkp${COLOR}"
	fi

	echo "[default]" >> ${config_file}
	echo "output = json" >> ${config_file}
	echo "region = ${region}" >> ${config_file}

	# backup if neccesary
	if [ -f ${credentials_file} ]; then
		mv ${credentials_file} "${credentials_file}.bkp"
		${logger} note "${credentials_file} already exists. A backup has been placed at ${YELLOW}${credentials_file}.bkp${COLOR}"
	fi

	echo "[default]" >> ${credentials_file}
	echo "aws_access_key_id = ${access_key}" >> ${credentials_file}
	echo "aws_secret_access_key = ${secret_key}" >> ${credentials_file}

	# bye
	${logger} success "aws configured."

}