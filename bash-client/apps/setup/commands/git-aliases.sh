#!/usr/bin/env bash

git-aliases() {

	${logger} title "Adding Git aliases"	

	git config --global alias.co checkout
	git config --global alias.br branch
	git config --global alias.ci commit
	git config --global alias.st status

	${logger} success "git st installed."
	${logger} success "git co installed."
	${logger} success "git br installed."
	${logger} success "git ci installed."
}