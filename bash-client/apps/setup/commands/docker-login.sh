#!/usr/bin/env bash

docker-login() {

    # keep args
    local args=$@

    # optional fields
    local username=$(${me} strings argument username $args)
    local password=$(${me} strings argument password $args)

    # check
    if [ -z "${username}" ] || [ -z "${password}" ]
    then
        help
        exit 1
    fi
    

    # go!
    ${logger} title "Setup Docker Profile"

    docker login -u "${username}" -p "${password}" docker.io > /dev/null 2>&1

    # check
    if [ ! $? -eq 0 ]; then
        ${logger} error "Cannot setup docker";
        exit 1
    fi

    # bye
    ${logger} success "docker configured."

}