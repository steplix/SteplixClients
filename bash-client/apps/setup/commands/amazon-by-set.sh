#!/usr/bin/env bash

amazon-by-set() {

    # keep args
    local args=$@

    # retrieve args
    local output=$(${me} strings argument output $args)
    local region=$(${me} strings argument region $args)
    local access_key=$(${me} strings argument access_key $args)
    local secret_key=$(${me} strings argument secret_key $args)

    # check
    if [ -z "${region}" ] || [ -z "${access_key}" ] || [ -z "${secret_key}" ]; then
        help
        exit 1
    fi

    if [ -z "${output}" ]; then
        output="json"
    fi
    

    # go!
    ${logger} title "Setup Amazon Profile"

    cd  # move to home

    local foldername="$(pwd)/.aws"
    local config_file="${foldername}/config"
    local credentials_file="${foldername}/credentials"

    # make folder if not exists
    if [ ! -d "${foldername}" ]; then
        mkdir -p "${foldername}"
    fi

    # backup if neccesary
    if [ -f ${config_file} ]; then
        mv ${config_file} "${config_file}.bkp"
        ${logger} note "${config_file} already exists. A backup has been placed at ${YELLOW}${config_file}.bkp${COLOR}"
    fi

    # backup if neccesary
    if [ -f ${credentials_file} ]; then
        mv ${credentials_file} "${credentials_file}.bkp"
        ${logger} note "${credentials_file} already exists. A backup has been placed at ${YELLOW}${credentials_file}.bkp${COLOR}"
    fi

    # set values on environment
    export AWS_REGION="${region}"
    export AWS_DEFAULT_REGION="${region}"

    # set values with aws configure command
    aws configure set aws_access_key_id "${access_key}"
    aws configure set aws_secret_access_key "${secret_key}"
    aws configure set region "${region}"
    aws configure set output "${output}"

    # bye
    ${logger} success "aws configured."

}