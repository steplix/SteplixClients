#!/usr/bin/env bash

git-user() {

	local name=""
	local email=""

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${@//--/|}"

	for element in "${my_array[@]}"; do

		# get the email
		if [[ $element == *"email="* ]]; then
			email=${element//email=/}
		fi

		# get the developer name
		if [[ $element == *"name="* ]]; then
			name=${element//name=/}
		fi

	done

	# check
	if [ -z "${email}" ] || [ -z "${name}" ]
	then
		help
		exit 1
	fi
	

	# go!
	${logger} title "Setup Git User"

	git config --global user.name "${name}"
	git config --global user.email "${email}"

	${logger} success "${name} <${email}> configured."
}