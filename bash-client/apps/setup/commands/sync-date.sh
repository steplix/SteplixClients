#!/usr/bin/env bash

sync-date() {

	# hello
	${logger} title "Updating date from Google Date Servers..."

	# go
	result=$(sudo date -s "$(wget -qSO- --max-redirect=0 google.com 2>&1 | grep Date: | cut -d' ' -f5-8)Z")

	# bye!
	${logger} success "date configured."
	${logger} note "${result}"
	
}