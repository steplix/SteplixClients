#!/usr/bin/env bash

ssh-key() {

	local mmddhhiiss=$(date '+%m%d%H%M%S')
	local private_key="steplix-${mmddhhiiss}"
	local public_key="${private_key}.pub"
	local ssh_path="$HOME/.ssh"
	local filepath="${ssh_path}/${private_key}"

	# hello
	${logger} title "Creating a ssh-key pair..."

	# create .ssh folder if not exists
	mkdir ${ssh_path} > /dev/null 2>&1

	# go
	ssh-keygen -t rsa -f ${filepath} -b 2048 -q -P ""

	if [ ! $? -eq 0 ]; then
		${logger} error "Cannot create the key";
		return
	fi

	# success!
	${logger} success "Your private was created at ${filepath}";
	${logger} success "Your public  was created at ${filepath}.pub";
	
	# the public key!
	${logger} note "this is the content of your public key:"
	
	public_key="${ssh_path}/${public_key}"
	echo -e "${YELLOW}"
	cat ${public_key}
	echo -e "${NOCOLOR}"
}