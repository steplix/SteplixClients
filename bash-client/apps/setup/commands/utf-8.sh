#!/usr/bin/env bash

utf-8() {
    sudo bash -c 'echo "LC_ALL=\"en_US.UTF-8\"" >> /etc/environment' > /dev/null 2>&1

	# check if everything is ok
	if [ $? -eq 0 ]; then
	    ${logger} success "en_US.UTF-8 defined at /etc/environment."
	else
	    ${logger} error "cannot setup en_US.UTF-8."
	fi
}