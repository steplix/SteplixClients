#!/usr/bin/env bash

signature() {


	# mandatory
	local name=""
	local email=""
	local role=""
	local mobile=""

	# optional
	local output=""

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${@//--/|}"

	for element in "${my_array[@]}"; do

		# get the name
		if [[ $element == *"name="* ]]; then
			name=${element//name=/}
		fi

		# get the email
		if [[ $element == *"email="* ]]; then
			email=${element//email=/}
		fi

		# get the role
		if [[ $element == *"role="* ]]; then
			role=${element//role=/}
		fi

		# get the mobile
		if [[ $element == *"mobile="* ]]; then
			mobile=${element//mobile=/}
		fi

		# get the output
		if [[ $element == *"output="* ]]; then
			output=${element//output=/}
		fi

	done

	# check
	if [ -z "${name}" ] || [ -z "${email}" ] || [ -z "${role}" ] || [ -z "${mobile}" ]
	then
		help
		exit 1
	fi

	# defaulting to file
	if [ -z "${output}" ]; then
		output="html"
	fi
	

	# go!
	${logger} title "Creating The Steplix Official HTML Signature..."

read -r -d '' html << EOM
<table style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;border-collapse:collapse">
  <tbody>
    <tr>
      <td style="padding:20px;width:120px">
        <p>
          <img src="http://steplix.com/assets/img/logo.png" alt="Steplix" style="height:30px; width: 120px;" height="30px" width="120px">
        </p>
      </td>
      <td style="padding:20px">
        <p style="font-size:14pt;margin:0px;color:rgb(0,0,0)">${name}</p>
        <p style="font-size:9pt;margin:0px;color:rgb(153,153,153)">${role}</p>
        <p style="font-size:9pt;margin:0px;color:rgb(153,153,153)">
          <a href="mailto:${email}" style="color:rgb(153,153,153)" target="_blank">${email}</a>
          &nbsp;•&nbsp;
          <a href="tel:${mobile}" style="color:rgb(153,153,153)" target="_blank">${mobile}</a>
        </p>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:9pt;padding:20px;color:rgb(204,204,204);border-top:1px solid rgb(239,238,238)">
        <a href="https://goo.gl/maps/t4eYJiQPgUxWpAyh9" style="font-size:9pt;color:rgb(204,204,204); text-decoration: none;">Costa Rica 4999 • Palermo, Buenos Aires, Argentina • C1414BSO</a><br><br>The content of this email is confidential and intended for the recipient specified in message only. It is strictly forbidden to share any part of this message with any third party, without a written consent of the sender. If you received this message by mistake, please reply to this message and follow with its deletion, so that we can ensure such a mistake does not occur in the future.
      </td>
    </tr>
  </tbody>
</table>
EOM


	# output
	if [ $output == 'display' ]; then

		echo -e "${YELLOW}${html}${NOCOLOR}\n"
		${logger} success "html signature created!"

	else

		local filepath="${HOME}/signature.html"
		echo "$html" > "${filepath}"

		${logger} success "html signature created at ${filepath}"
	fi

}