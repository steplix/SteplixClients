#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix customers application
# @author      	Steplix
# @description	customer scripts
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "finket-deploy" "finket-deploy-private-server")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/finket-deploy.sh"
source "${commands_path}/finket-deploy-private-server.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

