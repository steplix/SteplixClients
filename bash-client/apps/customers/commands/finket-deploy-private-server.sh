#!/usr/bin/env bash

# config
# ==============================================================================
availableEnvironments=("p" "production")
availableServers=("microservices-01" "authentication-01" "backoffice")
availableStrategies=("s3tag")
availableTypes=("nodejs" "reactjs" "nextjs")

# variable for exit function
function_result=""

finket-deploy-private-server() {

	# keep args
	local args=$@

	# mandatory args
	local application=$(${me} strings argument application $args)
	local environment=$(${me} strings argument environment $args)
	local type=$(${me} strings argument type $args)
	local working_dir=$(${me} strings argument working_dir $args)
	local strategy=$(${me} strings argument strategy $args)
	local tag=$(${me} strings argument tag $args)
	local server=$(${me} strings argument server $args)

	# fixed
	local username="columbia"
	local host="${server}"
	local keypath="/home/columbia/.ssh/${server}.pem"

	# precheck
	precheckArgument "${application}" "--application"
	precheckArgument "${environment}" "--environment"
	precheckArgument "${type}" "--type"
	precheckArgument "${working_dir}" "--working_dir"
	precheckArgument "${strategy}" "--strategy"
	precheckArgument "${tag}" "--tag"
	precheckArgument "${server}" "--server"

	precheckOption "--environment" "${environment}" "${availableEnvironments[@]}"
	precheckOption "--server" "${server}" "${availableServers[@]}"
	precheckOption "--strategy" "${strategy}" "${availableStrategies[@]}"
	precheckOption "--type" "${type}" "${availableTypes[@]}"

	precheckTagSintax "${tag}"

	precheckKeypath "${keypath}"

	# fixed artifact url
	local s3_tag="s3://steplix-applications/ctech3/${application}/${tag}.tar.gz"
	

	# create the commands
	local commands="steplix update; steplix server deploy --application=\"${application}\" --environment=\"${environment}\" --type=\"${type}\" --working_dir=\"${working_dir}\" --tag=\"${s3_tag}\" "

	# log
	${logger} title "Deploying application..."
	${logger} note "application > name: ${application}"
	${logger} note "application > environment: ${environment}"
	${logger} note "application > type: ${type}"
	${logger} note "application > tag: ${tag}"
	${logger} note "application > s3tag: ${s3_tag}"
	${logger} note "ssh > host: ${host}"
	${logger} note "ssh > username: ${username}"
	${logger} note "ssh > keypath: ${keypath}"
	${logger} note "ssh > working_dir: ${working_dir}"

	# run!
	ssh -i ${keypath} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${username}@${host} "${commands}"

}

precheckArgument() {

	local arg_item=$1
	local option_name=$2

	# by default, the argument
	function_result=${arg_item}

	# check
	if [ -z "${arg_item}" ]; then

		tooltip
		${logger} error "${option_name} is necessary."
		exit 1

	fi
}

precheckOption() {

	local command=$1
	local item=$2

	shift
	shift

	local options=("$@")

	if [[ ! " ${options[@]} " =~ " ${item} " ]]; then

		tooltip
		${logger} error "${command}=${item} it is not a valid option."
		exit 1

	fi

}

precheckTagSintax() {

	local version=$1
	local rx='^([0-9]+\.){2}(\*|[0-9]+)$'

	if [[ ! $version =~ $rx ]]; then
		tooltip
		${logger} error "--tag=${version} it is not a valid option. Ex. 0.0.2"
		exit 1
	fi

}

precheckKeypath() {

	local keypath=$1

	if [ ! -f "${keypath}" ]; then

		tooltip
		${logger} error "${keypath} does not exists."
		exit 1

	fi

}

tooltip() {
	echo ""
	${logger} title "Usage:"

	${logger} command "--application" "is mandatory."
	${logger} command "--environment" "is mandatory."
	${logger} command "--type" "is mandatory."
	${logger} command "--working_dir" "is mandatory (ex. /home/user/projects/)."
	${logger} command "--strategy" "is mandatory."
	${logger} command "--tag" "is mandatory (ex. 0.0.1)."
	${logger} command "--server" "is mandatory."
	echo ""
	${logger} title "available environments:"
	for t in ${availableEnvironments[@]}; do
		${logger} note "${t}"
	done
	
	echo ""
	${logger} title "available types:"
	for t in ${availableTypes[@]}; do
		${logger} note "${t}"
	done

	echo ""
	${logger} title "available strategy:"
	for t in ${availableStrategies[@]}; do
		${logger} note "${t}"
	done

	echo ""
	${logger} title "available servers:"
	for t in ${availableServers[@]}; do
		${logger} note "${t}"
	done
	echo ""
}
