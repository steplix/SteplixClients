#!/usr/bin/env bash

# variable for exit function
function_result=""

finket-deploy() {

	# precheck if this is a gitlab job
	${me} gitlab check
	if [ ! $? = 0 ]; then exit $?; fi

	# keep args
	local args=$@

	# environment variables (sometimes exists or not, depends on the end-user)
	local env_host=$(${me} strings trim ${ENV_PRODUCTION_HOST})
	local env_keypath=$(${me} strings trim ${ENV_PRODUCTION_KEY})
	local env_username=$(${me} strings trim ${ENV_SERVER_USERNAME})
	local env_working_dir=$(${me} strings trim ${ENV_SERVER_WORKING_DIR})
	local env_project_type=$(${me} strings trim ${ENV_PROJECT_TYPE})

	# always CI_PROJECT_NAME
	local application=$(${me} strings trim ${CI_PROJECT_NAME})

	# optional args
	local host=$(${me} strings argument host $args)
	local keypath=$(${me} strings argument keypath $args)
	local username=$(${me} strings argument username $args)
	local working_dir=$(${me} strings argument working_dir $args)
	local type=$(${me} strings argument type $args)
	local tag=$(${me} strings argument tag $args)
	local server=$(${me} strings argument server $args)

	# fixed
	local environment='p'
	local strategy='s3tag'

	# precheck project_type
	finket-deploy-precheckArgument "--type" "${type}" "${env_project_type}" 
	type="${function_result}"

	# precheck working_dir
	finket-deploy-precheckArgument "--working_dir" "${working_dir}" "${env_working_dir}"
	working_dir="${function_result}"

	# precheck ssh: keyfile
	finket-deploy-precheckArgument "--keypath" "${keypath}" "${env_keypath}" 
	keypath="${function_result}"

	if [ ! -f "${keypath}" ]; then
		${logger} error "${keypath} does not exists."
		exit 1
	fi

	# precheck ssh: hostname
	finket-deploy-precheckArgument "--host" "${host}" "${env_host}" 
	host="${function_result}"

	# precheck ssh: username
	finket-deploy-precheckArgument "--username" "${username}" "${env_username}" 
	username="${function_result}"

	# precheck server
	finket-deploy-precheckArgument "--server" "${server}" "unknown" 
	server="${function_result}"

	# precheck tag
	finket-deploy-precheckTag "${tag}"
	tag="${function_result}"

	# go!

	# change permissions
	sudo chmod 600 ${keypath}

	# create the commands
	local commands="steplix update; steplix customers finket-deploy-private-server --application=${application} --environment=${environment} --type=${type} --working_dir=${working_dir} --strategy=${strategy} --tag=${tag} --server=${server}"

	# log
	${logger} title "Deploying application..."
	${logger} note "application > name: ${application}"
	${logger} note "application > environment: ${environment}"
	${logger} note "application > type: ${type}"
	${logger} note "application > tag: ${tag}"
	${logger} note "ssh > host: ${host}"
	${logger} note "ssh > username: ${username}"
	${logger} note "ssh > keypath: ${keypath}"
	${logger} note "ssh > working_dir: ${working_dir}"

	# run!
	ssh -i ${keypath} -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ${username}@${host} "${commands}"

}

finket-deploy-precheckArgument() {

	local option_name=$1
	local arg_item=$2
	local env_item=$3

	# by default, the argument
	function_result=${arg_item}

	# check
	if [ -z "${arg_item}" ]; then

		# precheck environment variable
		if [ -z "${env_item}" ]; then
			${logger} error "${option_name} is necessary."
			finket-deploy-tooltip
			exit 1
		else
			function_result=${env_item}
		fi

	fi
}

finket-deploy-precheckTag() {

	local branch="${CI_COMMIT_REF_NAME}"
	local tag=$1

	# default
	function_result=""


	if [ -z "${tag}" ] && ([ "${branch}" == "master" ] || [ "${branch}" == "staging" ]); then

		# get the tag list
		git fetch --tags > /dev/null 2>&1

		# get latest version
		local latestVersion="$(git describe --tags $(git rev-list --tags --max-count=1))"

		if [ -z "${latestVersion}" ]; then

			# cannot depoy nonexistent tags
			${logger} error "This project does not have any tag yet. You cannot deploy from branch master without a tag."
			exit 1

		fi

		# well done!
		tag="${latestVersion}"


	elif [ ! -z "${tag}" ] && ([ "${branch}" == "master" ] || [ "${branch}" == "staging" ]); then

		# get the tag list
		git fetch --tags > /dev/null 2>&1

		# check if tag exists
		local version=$(git tag --list | grep "${tag}")

		if [ -z "${version}" ]; then

			# cannot depoy nonexistent tags
			${logger} error "The given tag ${tag} does not exists."
			exit 1

		fi

	fi

	# finally, did tag exists?
	if [ -z "${tag}" ]; then

		# cannot deploy nonexistent tags
		${logger} error "There is not tags."
		exit 1

	fi

	# bye
	function_result="${tag}"

}

finket-deploy-tooltip() {
	${logger} command "host" "is mandatory. Expected --host as argument or ENV_PRODUCTION_HOST as environment variable."
	${logger} command "keypath" "is mandatory. Expected --keypath as argument or ENV_PRODUCTION_KEY as environment variable."
	${logger} command "username" "is mandatory. Expected --username as argument or ENV_SERVER_USERNAME as environment variable."
	${logger} command "working_dir" "is mandatory. Expected --working_dir as argument or ENV_SERVER_WORKING_DIR as environment variable."
	${logger} command "type" "is mandatory. Expected --type as argument or ENV_PROJECT_TYPE as environment variable."
	${logger} command "server" "is mandatory. Expected --server as argument."
	${logger} command "tag" "is optional. Expected --tag as argument."
}
