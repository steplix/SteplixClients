#!/usr/bin/env bash

# Example:
#
# # JSON: {"name": "Nicolas"}
#
# steplix json put --json="${JSON}" --key='name' --value='Nico'
#
# # output (echo): '{"name": "Nicol"}'
#

put() {

    # keep args
    local args=$@

    # require args
    local json=$(${me} strings argument json $args)
    local key=$(${me} strings argument key $args)
    local value=$(${me} strings argument value $args)

    # check args
    if [ -z "${json}" ]; then
        ${logger} error "--json argument is required."
        exit 1
    fi

    if [ -z "${key}" ]; then
        ${logger} error "--key argument is required."
        exit 1
    fi

    if [ -z "${value}" ]; then
        ${logger} error "--value argument is required."
        exit 1
    fi

    echo ${json} | jq ".${key} = ${value}"
}
