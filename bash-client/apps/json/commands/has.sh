#!/usr/bin/env bash

# Example:
#
# # JSON: {"name": "Nicolas"}
#
# if $(steplix json has --json="${JSON}" --key='name'); then
#   echo "OK. name is defined"
# else 
#   echo "NO. name isn't defined"
# fi
#
# # output (echo): 'OK. name is defined'
#

has() {

    # keep args
    local args=$@

    # require args
    local json=$(${me} strings argument json $args)
    local key=$(${me} strings argument key $args)

    # check args
    if [ -z "${json}" ]; then
        ${logger} error "--json argument is required."
        exit 1
    fi

    if [ -z "${key}" ]; then
        ${logger} error "--key argument is required."
        exit 1
    fi

    local value=$(echo ${json} | jq .${key})

    if [ "${value}" == "null" ] || [ "${value}" == "undefined" ]; then
        return 1
    fi
    return 0
}
