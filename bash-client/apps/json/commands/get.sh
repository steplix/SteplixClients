#!/usr/bin/env bash

# Example:
#
# # JSON: {"name": "Nicolas"}
#
# steplix json get --json="${JSON}" --key='name'
#
# # output (echo): 'Nicolas'
#

get() {

    # keep args
    local args=$@

    # require args
    local json=$(${me} strings argument json $args)
    local key=$(${me} strings argument key $args)

    # check args
    if [ -z "${json}" ]; then
        ${logger} error "--json argument is required."
        exit 1
    fi

    if [ -z "${key}" ]; then
        ${logger} error "--key argument is required."
        exit 1
    fi

    echo ${json} | jq .${key}
}
