#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix json application
# @author      	Steplix
# @description	JSON Helper
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "get" "put" "del" "has")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/get.sh"
source "${commands_path}/put.sh"
source "${commands_path}/del.sh"
source "${commands_path}/has.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

