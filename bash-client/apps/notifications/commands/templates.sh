#!/usr/bin/env bash

templates() {

	# jq it is neccesary
	${me} install common

	# hello
	${logger} title "Getting available templates for notifications"

	# get
	json=$(curl --silent http://notifications.steplix.com:8000/templates)

	# check
	if [ -z "${json}" ]
	then
		${logger} error "cannot get templates from http://notifications.steplix.com:8000/templates"
		exit 1
	fi

	# show
	jq . <<< $(echo ${json})

	# bye
	${logger} success "Those are the available templates"

}