#!/usr/bin/env bash

slack() {

	local template=""

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${@//--/|}"

	for element in "${my_array[@]}"; do

		# get the template
		if [[ $element == *"template="* ]]; then
			template=${element//template=/}
		fi

	done

	# check mandatory
	if [ -z "${template}" ]
	then
		help
		exit 1
	fi

	# create the metadata
	local metadata=""
	local append=true

	for element in "${my_array[@]}"; do

		# template not neccesarry
		if [[ $element == *"template="* ]]; then
			append=false
		fi

		# append
		if ${append}; then

			IFS='|' read -r key value <<< "${element/=/|}"

			# only if not empty
			if [ ! -z "$key" ] && [ ! -z "${value}" ]; then

				# trim
				key=$(echo $key | sed -e 's/^[[:space:]]*//')
				value=$(echo $value | sed -e 's/^[[:space:]]*//')

				# append
				metadata="${metadata}, \"${key}\": \"${value}\""
			fi

		fi

		append=true

	done

	metadata="${metadata:2}"		# remove the leading ', '

	# create the data
	local data=$(cat <<-END
	    {
	    	"idTemplate": ${template},
	    	"idChannel": 4,
	    	"metadata": {
	    		${metadata}
	    	}
	    }
	END
	)

	# check
	# jq . <<< $(echo "${data}") > /dev/null 2>&1
	jq . <<< $(echo "${data}")

	if [ ! $? -eq 0 ]; then
	    ${logger} error "invalid json sintax"
	    echo "${data}"
	    exit 1
	fi

	# debug
	# jq . <<< $(echo "${data}")

	# send
	json=$(curl --silent --header "Content-Type: application/json" --request POST --data "${data}" http://notifications.steplix.com:8000/notifications)

	# check
	if [ -z "${json}" ]
	then
		${logger} error "cannot POST at http://notifications.steplix.com:8000/notifications"
		exit 1
	fi

	# bye
	${logger} success "slack message sent"
}