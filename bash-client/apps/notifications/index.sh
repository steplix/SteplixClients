#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix notifications application
# @author      	Steplix
# @description	Send notifications through sms, email, slack, etc.
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "email" "slack" "templates")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/email.sh"
source "${commands_path}/templates.sh"
source "${commands_path}/slack.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

