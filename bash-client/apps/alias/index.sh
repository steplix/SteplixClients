#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix alias application
# @author      	Steplix
# @description	Setup amazon, utf8, git, etc.
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "custom" "connect-mysql")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/custom.sh"
source "${commands_path}/connect-mysql.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

