#!/usr/bin/env bash

#setup connect-database [--password={password}] [--hostname=localhost] [--username=root] [--database=<none>]
#mysql -u root -pgonzalo notifications'

connect-mysql() {

	# vars
	local username="root"
	local password=""
	local database=""
	local hostname="localhost"

	# get args replacing '--' with '|'
	IFS='|' read -ra my_array <<< "${@//--/|}"

	for element in "${my_array[@]}"; do

		# get the database
		if [[ $element == *"database="* ]]; then
			database=${element//database=/}
		fi

		# get the username
		if [[ $element == *"username="* ]]; then
			username=${element//username=/}
		fi

		# get the password
		if [[ $element == *"password="* ]]; then
			password=${element//password=/}
		fi

		# get the hostname
		if [[ $element == *"hostname="* ]]; then
			hostname=${element//hostname=/}
		fi

	done


	# the value
	local the_command="mysql -u ${username} -p${password} -h ${hostname} ${database}"

	# go!
	${me} alias custom --name="connect-mysql" --command="${the_command}"
}