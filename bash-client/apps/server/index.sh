#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix server application
# @author      	Steplix
# @description	Deploy apps
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "deploy" "deploy-node" "deploy-react" "tag" "update" "status")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/deploy.sh"
source "${commands_path}/deploy-node.sh"
source "${commands_path}/deploy-react.sh"
source "${commands_path}/tag.sh"
source "${commands_path}/update.sh"
source "${commands_path}/status.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@

