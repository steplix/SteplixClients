#!/usr/bin/env bash

update() {

    # vars
    local basepath=""
    local application=""
    local environment=""

    # keep args
    local args=$@

    # get args replacing '--' with '|'
    IFS='|' read -ra my_array <<< "${@//--/|}"

    for element in "${my_array[@]}"; do

        # get the application
        if [[ $element == *"application="* ]]; then
            application=${element//application=/}
        fi

        # get the environment
        if [[ $element == *"environment="* ]]; then
            environment=${element//environment=/}
        fi

        # get the basepath
        if [[ $element == *"basepath="* ]]; then
            basepath=${element//basepath=/}
        fi

    done

    # precheck if everything is ok
    if [ -z "${application}" ]; then
        help
        return
    fi

    # precheck optional fields
    if [ -z "${basepath}" ]; then
        basepath=/home/$USER/src
    fi

    # precheck optional fields
    case "${environment}" in
        p | production) environment="master";;
        s | staging)    environment="staging";;
        t | testing)    environment="develop";;
    esac

    # precheck optional fields
    if [ -z "$environment" ]; then
        environment="develop"
    fi

    args="${args} --folderpath=${basepath}/${application}"
    args="${args} --branch=${environment}"

    # go!
    ${me} git update ${args}

}
