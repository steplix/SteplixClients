#!/usr/bin/env bash

version() {
	
	# variables
	local url=$1

	# check http code
	local response=$(curl --write-out %{http_code} --silent --output /dev/null ${url})

	if [ "${response}" == "000" ]; then
		echo "dead"
		return
	elif [ "${response}" == "404" ]; then
		echo "hidden"
		return
	fi

	# go go go
	local json=$(curl --silent "$1")

	# version exists?
	local version=$(echo "${json}" | jq --raw-output '.version')
	if [ "${version}" == "null" ]; then
		echo "n/a"
	else
		if [ -z "${version}" ]; then
			echo "dead"
		else
			echo ${version}
		fi
	fi
}

header() {

	local NORMAL=$(tput sgr0)
	local UNDERLINE=$(tput smul)

	local str1=$(printf '%40s' "")
	local str2=$(printf '%20s' "testing")
	local str3=$(printf '%20s' "staging")
	local str4=$(printf '%20s' "production")

	# go
	echo -e "${str1} \e[1m${str2}\e[21m \e[1m${str3}\e[21m \e[1m${str4}\e[21m"
}

table() {

	local application=$1
	local testing=$2
	local staging=$3
	local production=$4

	local color1="${YELLOW}"
	local color2=""
	local color3=""
	local color4=""

	# adjust color based on context
	if [ "${testing}" == "no-server" ] || [ "${testing}" == "hidden" ]; then
		color2="${GRAY}"
	elif [ "${testing}" == "dead" ]; then
		color2="${RED}"
	fi

	if [ "${staging}" == "no-server" ] || [ "${staging}" == "hidden" ]; then
		color3="${GRAY}"
	elif [ "${staging}" == "dead" ]; then
		color3="${RED}"
	fi

	if [ "${production}" == "no-server" ] || [ "${production}" == "hidden" ]; then
		color4="${GRAY}"
	elif [ "${production}" == "dead" ]; then
		color4="${RED}"
	else
		color4="${GREEN}"
	fi

	# right padding
	local str1=$(printf '%40s' "${application}")
	local str2=$(printf '%20s' "${testing}")
	local str3=$(printf '%20s' "${staging}")
	local str4=$(printf '%20s' "${production}")

	# go
	echo -e "${color1}${str1}${NOCOLOR} ${color2}${str2}${NOCOLOR} ${color3}${str3}${NOCOLOR} ${color4}${str4}${NOCOLOR}"
}

status() {

	# variables
	local filepath="${APP_PATH}/apps/server/commands/status.json"

	# precheck filepath existency
	if [ ! -f "${filepath}" ]; then
		${logger} error "${filepath} does not exists."
		exit 1
	fi

	# precheck jq is installed
	jq --version > /dev/null 2>&1
	if [ $? != 0 ]; then
		${logger} error "JQ is not installed. Execute steplix install common and try again."
		exit 1
	fi

	# precheck jq is installed
	curl --version > /dev/null 2>&1
	if [ $? != 0 ]; then
		${logger} error "CURL is not installed. Execute steplix install common and try again."
		exit 1
	fi

	# Go go go
	${logger} title "Fetching all applications..."

	# print header
	header

	# read status.json
	# ref. https://starkandwayne.com/blog/bash-for-loop-over-json-array-using-jq/
	for row in $(cat "${filepath}" | jq -r '.[] | @base64'); do

		_jq() {
			echo ${row} | base64 --decode | jq -r ${1}
		}

		local application=$(_jq '.application')
		local testing_url=$(_jq '.testing')
		local staging_url=$(_jq '.staging')
		local production_url=$(_jq '.production')
		local testing_version="no-server"
		local staging_version="no-server"
		local production_version="no-server"

		if [ -z ${application} ]; then
			${logger} error "Invalid status.json sintax. I got an object but there is not application attribute."
			exit 1
		fi

		# go go go!
		if [ "${testing_url}" != "null" ]; then
			testing_version=$(version "${testing_url}")
		fi

		if [ "${staging_url}" != "null" ]; then
			staging_version=$(version "${staging_url}")
		fi

		if [ "${production_url}" != "null" ]; then
			production_version=$(version "${production_url}")
		fi

		# trim everything
		application=$(${me} strings trim ${application})
		testing_version=$(${me} strings trim ${testing_version})
		staging_version=$(${me} strings trim ${staging_version})
		production_version=$(${me} strings trim ${production_version})

		# printout
		table ${application} ${testing_version} ${staging_version} ${production_version}

	done


}
