#!/usr/bin/env bash

tag() {

    # vars
    local basepath=""
    local application=""
    local tag="0.0.1"
    local currentpath=$(pwd)
    local result=0

    # get args replacing '--' with '|'
    IFS='|' read -ra my_array <<< "${@//--/|}"

    for element in "${my_array[@]}"; do

        # get the application
        if [[ $element == *"application="* ]]; then
            application=${element//application=/}
        fi

        # get the basepath
        if [[ $element == *"basepath="* ]]; then
            basepath=${element//basepath=/}
        fi

    done

    # precheck if everything is ok
    if [ -z "${application}" ]; then
        help
        return
    fi

    # precheck optional fields
    if [ -z "${basepath}" ]; then
        basepath=$HOME/src
    fi

    # precheck if folder if a git repository
    ${me} git check --folderpath=${basepath}/${application}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # hello
    ${logger} title "Creating tag file"

    # move to application folder
    cd ${basepath}/${application}

    # Try to create tag file
    if [ ! -z "$(git tag --list)" ]; then
        tag=$(git describe --tags $(git rev-list --tags --max-count=1))
    fi

    echo "{\"commit\":\"$(git rev-parse HEAD)\",\"version\":\"${tag}\",\"date\":\"$(date '+%A %d %B, %Y - %H:%M:%S')\"}" > tag.json

    if [ ! $? -eq 0 ]; then
        ${logger} error "Create tag file failed."
        cd ${currentpath}
        exit 1
    fi

    # Return to initial path (pwd)
    cd ${currentpath}

    # bye
    ${logger} success "Tag file created."

}
