#!/usr/bin/env bash

deploy() {

    # fingerprint
    local yyyymmddhhiiss=$(date '+%Y%m%d%H%M%S')

    # keep initial directory
    local initial_directory=$(pwd)

    # keep args
    local args=$@

    # arguments
    local application=$(${me} strings argument application $args)
    local environment=$(${me} strings argument environment $args)
    local working_dir=$(${me} strings argument working_dir $args)
    local strategy=$(${me} strings argument strategy $args)
    local branch=$(${me} strings argument branch $args)
    local type=$(${me} strings argument type $args)
    local tag=$(${me} strings argument tag $args)
    local bucket=$(${me} strings argument bucket $args)

    # other variables
    local project_path=""
    local tmp_project_path=""
    local git_source_url=""
    local git_branch=""

    # mandatory
    if [ -z ${application} ]; then
        ${logger} error "application argument is required."
        exit 1
    fi

    # defaults
    if [ -z ${environment} ]; then environment="t"; fi

    # defaults
    if [ -z ${working_dir} ]; then working_dir="$HOME/src"; fi

    # defaults
    if [ -z ${type} ]; then type="nodejs"; fi


    # update variables
    project_path="${working_dir}/${application}"
    tmp_project_path="${working_dir}/.${yyyymmddhhiiss}"

    # precheck environment syntax
    case "${environment}" in
        p | production)  environment="production";;
        s | staging)     environment="staging";;
        t | testing)     environment="testing";;
        d | development) environment="development";;
        *)               ${logger} error "environment '${environment}' is not allowed. Only 'p', 's', 't' or 'd' values are accepted."; exit 1;;
    esac

    # precheck type syntax
    case "${type}" in
        node    | nodejs)    type="nodejs";;
        react   | reactjs)   type="reactjs";;
        next    | nextjs)    type="nextjs";;
        nextjswebsite)       type="nextjswebsite";;
        ghost   | ghostjs)   type="ghost";;
        website | s3website) type="s3website";;
        php     | laravel)   type="laravel";;
        *)                   ${logger} error "type '${type}' is not allowed. Only 'nodejs', 'reactjs', 'nextjs', 'nextjswebsite', 'ghostjs', 's3website' or 'laravel' values are allowed."; exit 1;;
    esac

    # precheck strategy syntax
    if [ -z ${strategy} ]; then
        case "${environment}" in
            production  | staging) strategy="s3tag";;
            development | testing) strategy="git";;
        esac
    fi

    # precheck: change strategy based on react
    if [ "${environment}" == "testing" ]; then
        if [ "${type}" == "ghost" ] || [ "${type}" == "reactjs" ]; then
            strategy="s3tag";
        fi
    fi

    # create tmp project_path if it is all about a S3 bucket project because this script always required a previous installation
    if [ "${type}" == "s3website" ] || [ "${type}" == "nextjswebsite" ]; then

        setup-aws-for-deploy

        # precheck if folder path is a s3:// path
        if [[ "${bucket}" != s3://* ]]; then
            ${logger} error "${bucket} must start with s3://"
            exit 1
        fi

        # precheck if bucket exists
        aws s3 ls "${bucket}" > /dev/null 2>&1
        if [ ! $? -eq 0 ]; then
            ${logger} error "${bucket} does not exists."
            exit 1
        fi

        ${me} gitlab setup-amazon

        # Go go go!

        # hello!
        ${logger} title "Deploying ${application}..."

        if [ "${strategy}" == "local" ]; then
            project_path="${working_dir}"
        else 
            # create the empty folder
            if [ ! -d "${project_path}" ]; then
                
                mkdir "${project_path}"
                
                if [ $? -eq 0 ]; then
                    ${logger} success "empty ${project_path} folder created."
                else
                    ${logger} error "command failed: mkdir ${project_path}"
                    exit 1
                fi
            fi
        
            # precheck project is placed where the user said
            if [ ! -d "${project_path}" ]; then
                ${logger} error "${project_path} does not exists."
                exit 1
            fi

            # make a tmp folder
            mkdir "${tmp_project_path}"
            ${logger} success "${tmp_project_path} folder created."

            # First step, downloading the project based on the selected strategy.
            download ${strategy} ${application} ${environment} ${project_path} ${tmp_project_path} ${branch} ${tag}
            if [ ! $? -eq 0 ]; then clean ${initial_directory} ${tmp_project_path}; exit 1; fi

            # Second step, prepare the project based on the selected type.
            prepare ${type} ${application} ${environment} ${project_path} ${tmp_project_path}
            if [ ! $? -eq 0 ]; then clean ${initial_directory} ${tmp_project_path}; exit 1; fi

            # Third step, generate a backup in case you need a rollback.
            backup ${application} ${working_dir} ${project_path}
            if [ ! $? -eq 0 ]; then clean ${initial_directory} ${tmp_project_path}; exit 1; fi
        fi
    fi

    # Fourth step, deploy.
    deploy-by-type ${type} ${application} ${environment} ${working_dir} ${project_path} ${tmp_project_path} ${bucket} ${strategy}
    if [ ! $? -eq 0 ]; then clean ${initial_directory} ${tmp_project_path}; exit 1; fi

    # clean all
    clean ${initial_directory} ${tmp_project_path}

    # bye
    ${logger} success "${application} deployed."

}

clean() {
    cd "${1}"
    rm -rf "${2}"
}

download() {
    local strategy="${1}"
    local application="${2}"
    local environment="${3}"
    local project_path="${4}"
    local tmp_project_path="${5}"
    local branch="${6}"
    local tag="${7}"

    # be careful because when we pass empty variables, $N moves 1 space to left
    if [ "${strategy}" == "s3tag" ]; then
        tag="${6}"

        if [ -z "${tag}" ]; then
            ${logger} error "strategy '${strategy}' requires a tag. Empty tag given." 
            exit 1
        fi
    fi

    # Go!
    if [ "${strategy}" == "git" ]; then

        # Deploy application with GIT strategy
        download-by-git "${application}" "${environment}" "${project_path}" "${tmp_project_path}" "${branch}"

    elif [ "${strategy}" == "s3tag" ]; then

        # Deploy application with S3 tag strategy
        download-by-s3tag "${application}" "${environment}" "${project_path}" "${tmp_project_path}" "${tag}"

    else
        # strategy not found
        ${logger} error "strategy '${strategy}' is not allowed. Only 'git' or 's3tag' values are allowed." 
        exit 1
    fi

}

download-by-git() {

    local application="${1}"
    local environment="${2}"
    local project_path="${3}"
    local tmp_project_path="${4}"
    local branch="${5}"
    local git_source_url=""

    # precheck git
    ${me} git check --folderpath=${project_path}
    if [ ! $? -eq 0 ]; then exit 1; fi

    # resolve branch
    if [ -z "${branch}" ]; then
        case "${environment}" in
            production) branch="master";;
            staging) branch="master";;
            testing) branch="develop";;
            development) branch="develop";;
        esac
    fi

    # go to git project
    cd "${project_path}"

    # update variables
    git_source_url=$(git config --get remote.origin.url)

    # move to the folder
    cd "${tmp_project_path}"

    # clone the project
    git clone "${git_source_url}" > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then ${logger} error "${application} can't be cloned."; exit 1; fi
    ${logger} success "${tmp_project_path}/${application}.git project cloned."

    # move to the folder
    cd "${tmp_project_path}/${application}"

    # set source code to staging branch
    git checkout "${branch}" > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then ${logger} error "${application} does not have a branch '${branch}' for ${env} environment."; exit 1; fi

}

download-by-s3tag() {

    local application="${1}"
    local environment="${2}"
    local project_path="${3}"
    local tmp_project_path="${4}"
    local tag="${5}"

    # precheck amazon
    ${me} amazon check

    # precheck file exists
    aws s3 ls "${tag}" > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then 
        ${logger} error "${tag} does not exists."; 
        exit 1; 
    fi

    # move to the folder
    cd "${tmp_project_path}"

    # download tag from S3
    aws s3 cp "${tag}" . > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then 
        ${logger} error "command failed: aws s3 cp ${tag}.";
        exit 1;
    fi

    # file exists?
    local filename="$(basename -- ${tag})"
    if [ ! -f "${filename}" ]; then
        ${logger} error "${filename} does not exists.";
        exit 1;
    fi

    # mkdir application folder
    mkdir "${tmp_project_path}/${application}" > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then 
        ${logger} error "command failed: mkdir ${tmp_project_path}/${application}";
        exit 1;
    fi

    # extract
    tar -xzf "${filename}" -C "${tmp_project_path}/${application}" > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then 
        ${logger} error "command failed: tar -xzf ${filename} -C ${tmp_project_path}/${application}"; 
        exit 1; 
    fi

    # clean
    rm "${filename}"

    # bye
    ${logger} success "${tmp_project_path}/${application} project downloaded from S3."
}

prepare() {
    local type="${1}"
    local application="${2}"
    local environment="${3}"
    local project_path="${4}"
    local tmp_project_path="${5}"

    if [ "${type}" = "nodejs" ]; then

        # Prepare application for "nodejs" type
        prepare-environment ${application} ${environment} ${project_path} ${tmp_project_path}
        prepare-nodejs ${application} ${environment} ${project_path} ${tmp_project_path}

    elif [ "${type}" = "nextjs" ]; then

        # Prepare application for "nodejs" type
        prepare-environment ${application} ${environment} ${project_path} ${tmp_project_path}
        prepare-nodejs ${application} ${environment} ${project_path} ${tmp_project_path}
        prepare-reactjs ${application} ${environment} ${project_path} ${tmp_project_path}

    elif [ "${type}" = "nextjswebsite" ]; then

        # Prepare application for "nodejs" type
        prepare-environment ${application} ${environment} ${project_path} ${tmp_project_path}
        prepare-nodejs ${application} ${environment} ${project_path} ${tmp_project_path} 'false'
        prepare-nextjswebsite ${application} ${environment} ${project_path} ${tmp_project_path}

    elif [ "${type}" = "reactjs" ]; then

        # Prepare application for "reactjs" type
        prepare-environment ${application} ${environment} ${project_path} ${tmp_project_path}

    elif [ "${type}" = "ghost" ]; then

        # Prepare application for "ghost" type
        prepare-environment ${application} ${environment} ${project_path} ${tmp_project_path}

    elif [ "${type}" = "s3website" ]; then

        # Prepare application for "s3website" type
        prepare-environment ${application} ${environment} ${project_path} ${tmp_project_path}

    elif [ "${type}" = "laravel" ]; then

        # Prepare application for "laravel" type
        prepare-environment ${application} ${environment} ${project_path} ${tmp_project_path}
        prepare-laravel ${application} ${environment} ${project_path} ${tmp_project_path}

    else
        # type not found
        ${logger} error "type '${type}' is not allowed. Only 'nodejs', 'nextjs', 'reactjs', 'ghost', 's3website' or 'laravel' values are allowed." 
        exit 1
    fi
}

prepare-environment() {

    local application="${1}"
    local environment="${2}"
    local project_path="${3}"
    local tmp_project_path="${4}"

    # move to the folder
    cd "${tmp_project_path}/${application}"
    if [ ! $? -eq 0 ]; then 
        ${logger} error "command failed: cd ${tmp_project_path}/${application}";
        exit 1;
    fi

    # check environment name
    case "${environment}" in
        development) environment="local";;
    esac

    # check if .env file exist for environment
    if [ -f "${tmp_project_path}/${application}/.env.${environment}" ]; then

        # try to copy .env file
        cp "${tmp_project_path}/${application}/.env.${environment}" "${tmp_project_path}/${application}/.env"
        if [ ! $? -eq 0 ]; then ${logger} error "Can't be copy ${tmp_project_path}/${application}/.env.${environment} to ${tmp_project_path}/${application}/.env"; exit 1; fi
        ${logger} success "${tmp_project_path}/${application}/.env file created."

    # check for existing .env file
    elif [ -f "${project_path}/.env" ]; then
        
        cp "${project_path}/.env" "${tmp_project_path}/${application}/.env"
        if [ ! $? -eq 0 ]; then ${logger} error "command file: cp ${project_path}/.env ${tmp_project_path}/${application}/.env"; exit 1; fi
        ${logger} success "${tmp_project_path}/${application}/.env file created."

    fi

    # clean unnecesary .env files
    case "${environment}" in
        p | production) rm -f .env.testing .env.staging;;
        s | staging)    rm -f .env.production .env.testing;;
        t | testing)    rm -f .env.production .env.staging;;
    esac

}

prepare-nodejs() {

    local application="${1}"
    local environment="${2}"
    local project_path="${3}"
    local tmp_project_path="${4}"
    local clean_install="${5:true}"
    local result=0

    # move to the folder
    cd "${tmp_project_path}/${application}"
    if [ ! $? -eq 0 ]; then 
        ${logger} error "command failed: cd ${tmp_project_path}/${application}";
        exit 1;
    fi

    # try to install dependencies
    if [ "${clean_install}" == "true" ]; then
        npm ci > /dev/null 2>&1
        result=$?
    else
        npm i > /dev/null 2>&1
        result=$?
    fi

    if [ ! $result -eq 0 ]; then ${logger} error "Installing dependencies failed."; exit $result; fi
    ${logger} success "Dependencies installed."

}

prepare-nextjswebsite() {

    local application="${1}"
    local environment="${2}"
    local project_path="${3}"
    local tmp_project_path="${4}"

    # move to the folder
    cd "${tmp_project_path}/${application}"
    if [ ! $? -eq 0 ]; then 
        ${logger} error "command failed: cd ${tmp_project_path}/${application}";
        exit 1;
    fi

    # try to create build
    NODE_ENV=${environment} npm run release
    if [ ! $? -eq 0 ]; then ${logger} error "Creating release failed."; exit 1; fi
    ${logger} success "Release created."

}

prepare-reactjs() {

    local application="${1}"
    local environment="${2}"
    local project_path="${3}"
    local tmp_project_path="${4}"

    # move to the folder
    cd "${tmp_project_path}/${application}"
    if [ ! $? -eq 0 ]; then 
        ${logger} error "command failed: cd ${tmp_project_path}/${application}";
        exit 1;
    fi

    # try to create build
    NODE_ENV=${environment} npm run build > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then ${logger} error "Creating build failed."; exit 1; fi
    ${logger} success "Build created."

}

prepare-laravel() {

    local application="${1}"
    local environment="${2}"
    local project_path="${3}"
    local tmp_project_path="${4}"

    # move to the folder
    cd "${tmp_project_path}/${application}"
    if [ ! $? -eq 0 ]; then 
        ${logger} error "command failed: cd ${tmp_project_path}/${application}";
        exit 1;
    fi

    # try to create build
    composer install > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then ${logger} error "Installing dependencies failed."; exit 1; fi
    ${logger} success "Dependencies installed."

}

backup() {

    local application="${1}"
    local working_dir="${2}"
    local project_path="${3}"
    local extension=".tar.gz"

    # Do backup folder exists?
    if [ ! -d "${working_dir}/.backup" ]; then
        mkdir "${working_dir}/.backup"
        ${logger} success "${working_dir}/.backup folder created."
    fi

    tar -czf "${working_dir}/.backup/${application}${extension}" --absolute-name "${project_path}"
    if [ ! $? -eq 0 ]; then ${logger} error "can't tar ${working_dir}/.backup/${application}${extension} from ${project_path}"; exit 1; fi

    # clean
    sudo rm -rf "${project_path}"

    # bye
    ${logger} success "${project_path} moved to ${working_dir}/.backup/${application}"

}

deploy-by-type() {

    local type="${1}"
    local application="${2}"
    local environment="${3}"
    local working_dir="${4}"
    local project_path="${5}"
    local tmp_project_path="${6}"
    local bucket="${7}"
    local strategy="${8}"

    if [ "${strategy}" != "local" ]; then
        # move the new application version to the working directory
        mv -vf "${tmp_project_path}/${application}" "${working_dir}" > /dev/null 2>&1
        if [ ! $? -eq 0 ]; then ${logger} error "can't move ${tmp_project_path}/${application} to ${working_dir}"; exit 1; fi
        ${logger} success "${tmp_project_path}/${application} moved to ${working_dir}"

        # clean
        rm -rf "${tmp_project_path}"
    fi 

    
    if [ "${type}" = "nodejs" ] || [ "${type}" = "nextjs" ] || [ "${type}" = "reactjs" ] || [ "${type}" = "ghost" ]; then

        # Deploy application for "nodejs" type
        deploy-with-nodejs ${application} ${environment} ${project_path}

    elif [ "${type}" = "nextjswebsite" ]; then

        # Prepare application for "nextjswebsite" type
        deploy-with-nextjswebsite ${project_path} ${bucket}

    elif [ "${type}" = "s3website" ]; then

        # Prepare application for "s3website" type
        deploy-with-s3website ${project_path} ${bucket}

    elif [ "${type}" = "laravel" ]; then

        # Prepare application for "laravel" type
        deploy-with-laravel ${project_path}

    else
        # type not found
        ${logger} error "type '${type}' is not allowed. Only 'nodejs', 'nextjs', 'nextjswebsite', 'reactjs', 'ghost', 's3website' or 'laravel' values are allowed." 
        exit 1
    fi

}

deploy-with-nodejs() {

    local application="${1}"
    local environment="${2}"
    local project_path="${3}"

    # move to the folder
    cd "${project_path}"

    # restart the app
    ${me} pm2 start --application=${application} --environment=${environment}

}

deploy-with-nextjswebsite() {

    local folder="${1}"
    local bucket="${2}"

    ls "${folder}"

    setup-aws-for-deploy

    # clean bucket
    ${me} amazon empty-bucket --bucket="${bucket}"

    # copy to bucket
    ${me} amazon copy-folder-to-s3 --folder="${folder}/out" --bucket="${bucket}"
}

deploy-with-s3website() {

    local folder="${1}"
    local bucket="${2}"

    setup-aws-for-deploy

    # clean bucket
    ${me} amazon empty-bucket --bucket="${bucket}"

    # copy to bucket
    ${me} amazon copy-folder-to-s3 --folder="${folder}" --bucket="${bucket}"
}

deploy-with-laravel() {

    local project_path="${1}"

    # move to the folder
    cd "${project_path}"

    # optimize all
    php artisan optimize

    # permissions
    sudo chmod -R 777 storage
    sudo chmod -R 777 bootstrap

}

setup-aws-for-deploy() {

    # precheck amazon
    ${me} amazon check

    local env_region=$(${me} strings trim ${AWS_DEPLOY_REGION})
    local env_access_key=$(${me} strings trim ${AWS_DEPLOY_ACCESS_KEY})
    local env_secret_key=$(${me} strings trim ${AWS_DEPLOY_SECRET_KEY})

    # default fields
    if [ -z "${env_region}" ]; then
        env_region=$(${me} strings trim ${AWS_REGION})
    fi

    if [ -z "${env_access_key}" ]; then
        env_access_key=$(${me} strings trim ${AWS_ACCESS_KEY})
    fi

    if [ -z "${env_secret_key}" ]; then
        env_secret_key=$(${me} strings trim ${AWS_SECRET_KEY})
    fi

    # configure other credentials
    ${me} setup amazon --region=${env_region} --access_key=${env_access_key} --secret_key=${env_secret_key} > /dev/null 2>&1

}
