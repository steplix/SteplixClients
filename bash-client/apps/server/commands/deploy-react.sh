#!/usr/bin/env bash

deploy-react() {

    # vars
    local branch=""
    local basepath=""
    local application=""
    local currentpath=$(pwd)
    local result=0

    # keep args
    local args=$@

    # get args replacing '--' with '|'
    IFS='|' read -ra my_array <<< "${@//--/|}"

    for element in "${my_array[@]}"; do

        # get the application
        if [[ $element == *"application="* ]]; then
            application=${element//application=/}
        fi

        # get the basepath
        if [[ $element == *"basepath="* ]]; then
            basepath=${element//basepath=/}
        fi

    done

    # precheck if everything is ok
    if [ -z "${application}" ]; then
        help
        return
    fi

    # precheck optional fields
    if [ -z "${basepath}" ]; then
        basepath=$HOME/src
    fi

    # precheck optional fields
    if [ ! -z "$branch" ]; then
        args="${args} --branch=${branch}"
    fi

    args="${args} --folderpath=${basepath}/${application}"
    args="${args} --basepath=${basepath}"

    # update git repository
    ${me} git update ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # precheck pm2 dependecies
    ${me} pm2 check ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # go!
    ${logger} title "Deploying react app"

    # move to application folder
    cd ${basepath}/${application}

    # try to install dependencies
    ${logger} step "Install dependencies..."

    # Remote current node_modules folder
    rm -rf node_modules > /dev/null 2>&1
    # Install dependecies by package-lock.json
    npm ci > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "Install dependencies failed."
        cd ${currentpath}
        exit 1
    fi

    ${logger} success "Dependencies Installed."

    # try to build application
    ${logger} step "Building application..."

    # Remote current build folder
    rm -rf build > /dev/null 2>&1
    # Create a new build folder
    npm run build > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "Build application failed."
        cd ${currentpath}
        exit 1
    fi

    ${logger} success "Application built correctly."

    # try to create tag file
    ${me} server tag ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # try to start application with pm2
    ${me} pm2 start ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # Return to initial path (pwd)
    cd ${currentpath}

}
