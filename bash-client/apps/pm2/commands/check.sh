#!/usr/bin/env bash

check() {

    # vars
    local pm2file=""
    local basepath=""
    local application=""
    local currentpath=$(pwd)

    # get args replacing '--' with '|'
    IFS='|' read -ra my_array <<< "${@//--/|}"

    for element in "${my_array[@]}"; do

        # get the application
        if [[ $element == *"application="* ]]; then
            application=${element//application=/}
        fi

        # get the pm2file
        if [[ $element == *"pm2file="* ]]; then
            pm2file=${element//pm2file=/}
        fi

        # get the basepath
        if [[ $element == *"basepath="* ]]; then
            basepath=${element//basepath=/}
        fi

    done

    # precheck if everything is ok
    if [ -z "${application}" ]; then
        help
        return
    fi

    # precheck optional fields
    if [ -z "${basepath}" ]; then
        basepath=$HOME/src
    fi

    # precheck optional fields
    if [ -z "$pm2file" ]; then
        pm2file="ecosystem.config.js"
    fi

    # precheck if pm2 is installed
    pm2 -v > /dev/null 2>&1
    if [ ! $? -eq 0 ]; then
        ${logger} error "pm2 is not installed."
        cd "${currentpath}"
        exit 1

    fi

    # precheck directory
    if [ ! -d ${basepath}/${application} ]; then
        ${logger} error "Directory (${basepath}/${application}) does not exists."
        cd ${currentpath}
        exit 1
    fi

    # move to application folder
    cd ${basepath}/${application}

    # precheck file existance
    if [ ! -f "$pm2file" ]; then
        ${logger} error "PM2 file (${pm2file}) does not exists."
        cd ${currentpath}
        exit 1
    fi

    # Return to initial path (pwd)
    cd ${currentpath}

}
