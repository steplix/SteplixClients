#!/usr/bin/env bash

start() {

    # vars
    local pm2file=""
    local basepath=""
    local application=""
    local environment=""
    local currentpath=$(pwd)
    local result=0

    # keep args
    local args=$@

    # get args replacing '--' with '|'
    IFS='|' read -ra my_array <<< "${@//--/|}"

    for element in "${my_array[@]}"; do

        # get the application
        if [[ $element == *"application="* ]]; then
            application=${element//application=/}
        fi

        # get the environment
        if [[ $element == *"environment="* ]]; then
            environment=${element//environment=/}
        fi

        # get the pm2file
        if [[ $element == *"pm2file="* ]]; then
            pm2file=${element//pm2file=/}
        fi

        # get the basepath
        if [[ $element == *"basepath="* ]]; then
            basepath=${element//basepath=/}
        fi

    done

    # precheck if everything is ok
    if [ -z "${application}" ]; then
        help
        return
    fi

    # precheck optional fields
    if [ -z "${basepath}" ]; then
        basepath=$HOME/src
    fi

    # precheck optional fields
    case "${environment}" in
        p | production) branch="master";;
        s | staging)    branch="staging";;
        t | testing)    branch="develop";;
    esac

    # precheck optional fields
    if [ -z "$environment" ]; then
        environment="testing"
    fi

    # precheck optional fields
    if [ -z "$pm2file" ]; then
        pm2file="ecosystem.config.js"
    fi

    # precheck pm2 dependencies
    ${me} pm2 check ${args}
    result=$?

    if [ ! ${result} -eq 0 ]; then
        cd ${currentpath}
        exit ${result}
    fi

    # hello
    ${logger} title "Starting app on PM2"

    # move to application folder
    cd ${basepath}/${application}

    # Try to start on PM2
    NODE_ENV=${environment} pm2 start ${pm2file} --env ${environment} > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "Start app on PM2 failed."
        cd ${currentpath}
        exit 1
    fi

    # Try to save PM2 current list apps
    pm2 save > /dev/null 2>&1

    if [ ! $? -eq 0 ]; then
        ${logger} error "Save PM2 status failed."
        cd ${currentpath}
        exit 1
    fi

    # Return to initial path (pwd)
    cd ${currentpath}

    # bye
    ${logger} success "Application ${application} started through PM2 management system."

}
