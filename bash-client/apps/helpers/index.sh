#!/usr/bin/env bash

# ==========================================================
# @application  Steplix helpers
# @author       Steplix
# @description  useful commands for helpers
# ==========================================================

# load parent functions
# ==========================================================
source "$( dirname $( dirname "${BASH_SOURCE[0]}" ) )/.base/parent.sh"


# available commands
# ==========================================================
declare -a available_commands=("help" "semver-increase")


# this application commands
# ==========================================================
commands_path="$(dirname "${BASH_SOURCE[0]}")/commands"

source "${commands_path}/semver-increase.sh"


# Run!
# ==========================================================
check_arguments $@
execute $@
