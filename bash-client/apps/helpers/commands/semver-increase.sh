#!/usr/bin/env bash

semver-increase() {
    # keep args
    local args=$@

    # retrieve args
    local version=$(${me} strings argument version $args)
    local level=$(${me} strings argument level $args)
    local value=$(${me} strings argument value $args)
    local majorPart=0
    local minorPart=0
    local patchPart=0

    # default args
    if [ -z "${version}" ]; then
        version="0.0.0"
    fi
    if [ -z "${level}" ]; then
        level="patch"
    fi
    if [ -z "${value}" ]; then
        value=1
    fi

    # break down the version number into it's components
    local parts=(${version//./ })
    majorPart="${parts[0]}"
    minorPart="${parts[1]}"
    patchPart="${parts[2]}"

    # check paramater to see which number to increase
    case "${level}" in
        M | MAJOR | major) majorPart=$((($majorPart + 0) + ($value + 0))); minorPart=0; patchPart=0;;
        m | MINOR | minor) minorPart=$((($minorPart + 0) + ($value + 0))); patchPart=0;;
                        *) patchPart=$((($patchPart + 0) + ($value + 0)));;
    esac

    # echo the new version number
    echo "${majorPart}.${minorPart}.${patchPart}"
}
