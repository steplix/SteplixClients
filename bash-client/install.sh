#!/usr/bin/env bash

# ==========================================================
# @application 	Steplix bash client installer
# @author      	Steplix
# ==========================================================

# 0 - variables

	hhiiss=$(date '+%H%M%S')
	application_path="$HOME/.steplix"
	download_path="$HOME/steplix-${hhiiss}"
	current_path=$(pwd)


# 1 - this script requires git installed

	git --version > /dev/null 2>&1

	if [ ! $? -eq 0 ]; then
	    apt-get install -y git
	fi

# 2 - check if steplix already installed

	steplix version > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		rm -rf "${application_path}"
		sudo rm /usr/local/bin/steplix
	fi

# 3 - clone the SteplixClients project

	rm -rf "${download_path}"
	git clone https://gitlab.com/steplix/SteplixClients.git "${download_path}"

# 4 - Create TAG file

	cd "${download_path}"

	git fetch --tags > /dev/null 2>&1
	latestVersion="$(git describe --tags $(git rev-list --tags --max-count=1))"
	echo "${latestVersion}" > "${download_path}/bash-client/TAG"

# 5 - move app files to user directory .steplix

	sudo mv "${download_path}/bash-client" "${application_path}"

# 6 - create link to steplix bash script on /usr/local/bin

	sudo ln -s "${application_path}/steplix" /usr/local/bin

# 7 - check if is installed ok

	steplix version  > /dev/null 2>&1

	if [ ! $? -eq 0 ]; then
	    echo "Error: cannot install steplix."
	else
		steplix logger success "well done!"
		steplix version
	fi

# 8 - clean

	rm -rf "${download_path}"
	cd "${current_path}"
