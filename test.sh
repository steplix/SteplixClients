#!/usr/bin/env bash

# ==========================================================
# @application  Inspirated for test any script
# @author       Nico Molina
#
# @example      All example run on your SteplixClients directory
#
#               * Run logger message:
#                   ./test.sh logger info "Hola"
#
# ----------------------------------------------------------
#
#               * Run gitlab command:
#                   CI_PROJECT_NAME=MyProyectName ./test.sh gitlab docker-image-to-ecr --forcebranch=true --forcetag=true
# ==========================================================

# Script (SteplixClients) directory
STEPLIX_PATH="${PWD}/bash-client/steplix"

# tester-executer
tester-executer () {
  CUSTOM_APP_PATH="${PWD}/bash-client" ${STEPLIX_PATH} $@
}

# Welcome message
tester-executer logger title "Welcome to the Steplix test script"
tester-executer logger newline

# Go go go
tester-executer $@

# Bye
